use crate::qt;
use parsegame::Parser;
use std::io::{self, BufRead, BufReader};
use std::path::PathBuf;
use std::thread::sleep;
use std::time::{Duration, Instant};
use tailf::Tailf;
use trackers::{DeckTracker, DeckList};

pub struct GameWatch {
    power_log_path: PathBuf,
    tracker: DeckTracker,
}

// TODO try to remove Clone here: will cause lifetime issues
// one way is to split it into DeckVecs in send_track_results, and send to corresponding handlers
// via HashMap
#[derive(Default, Clone)]
pub struct TrackResults {
    pub our_natives: DeckList,
    pub our_foreigns: DeckList,
    pub their_natives: DeckList,
    pub their_deck_foreigns: DeckList,
    pub their_hand_foreigns: DeckList,
}

impl GameWatch {
    pub fn new(power_log_path: PathBuf) -> GameWatch {
        GameWatch {
            power_log_path,
            tracker: DeckTracker::new(),
        }
    }

    pub fn exec(&mut self) -> io::Result<()> {
        // actually, Result<!> atm
        let tail = Tailf::watch(self.power_log_path.clone());
        // TODO clone() is not cool. let Tailf accept Path
        let mut parser = Parser::new();

        let mut reader = BufReader::new(tail);
        let mut buf = String::new();
        let mut instant = Instant::now(); // last buffer update
        let mut events_happened = false;
        loop {
            let mut input_depleted = false;
            let n = reader.read_line(&mut buf)?;
            if n > 0 {
                instant = Instant::now();
                if buf.ends_with('\n') {
                    parser.feed_line(&buf);
                    buf.clear();
                }
            } else {
                if buf.is_empty() && instant.elapsed() > Duration::new(0, 500_000_000) {
                    // if 0.5s have passed, notify parser
                    parser.feed_pause();
                }
                let status = reader.get_mut().consume_status()?;
                if status.is_truncated() {
                    parser.feed_end();
                    if !buf.is_empty() {
                        debug_assert!(!buf.ends_with('\n'));
                        log::warn!("Unfinished line: {:?}", buf);
                        buf.clear();
                    }
                } else {
                    input_depleted = true;
                }
            }

            while let Some(event) = parser.pop() {
                self.tracker.send(&event);
                events_happened = true;
            }
            if events_happened && input_depleted {
                self.update_deck_lists();
                events_happened = false;
            }
            if input_depleted {
                sleep(Duration::new(0, 50_000_000)); // wait 50ms for more data
            }
        }
    }

    fn update_deck_lists(&mut self) {
        if self.tracker.sides_defined() {
            //println!("====================");
            let natives = self.tracker.our_natives_outside_deck();
            //natives.print();
            let foreings = self.tracker.our_foreigns_in_deck();
            //if !foreings.is_empty() {
            //    println!("------foreings:------");
            //    foreings.print();
            //}
            let their_natives = self.tracker.their_natives_out();
            let their_deck_foreigns = self.tracker.their_foreigns_in_deck();
            //their_deck_foreigns.print();
            //println!("--------------------");
            let their_hand_foreigns = self.tracker.their_foreigns_in_hand();
            //their_hand_foreigns.print();
            //their_natives.print();
            let results = TrackResults {
                our_natives: natives,
                our_foreigns: foreings,
                their_natives: their_natives,
                their_deck_foreigns: their_deck_foreigns,
                their_hand_foreigns: their_hand_foreigns,
            };
            qt::send_track_results(results);
        }
    }
}
