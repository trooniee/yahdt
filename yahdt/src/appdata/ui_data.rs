use atomicwrites::*;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::error::Error;
use std::fs::{self, File};
use std::io::{self, Read, Write};
use std::path::{Path, PathBuf};

pub struct UiData {
    path: PathBuf,
    json: JsonFile,
}

#[derive(Default, Serialize, Deserialize)]
struct JsonFile {
    windows: HashMap<String, Window>,
}

#[derive(Serialize, Deserialize)]
pub struct Window {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
}

impl UiData {
    pub fn from_dir(path: &Path) -> Self {
        let mut r = Self {
            path: path.to_owned(),
            json: Default::default(),
        };
        if let Err(err) = r.load() {
            match err.downcast_ref::<io::Error>() {
                Some(ref io_err) if io_err.kind() == io::ErrorKind::NotFound => {}
                _ => log::error!("Error loading UiData: {}", err),
            }
        };
        r
    }

    fn load(&mut self) -> Result<(), Box<dyn Error>> {
        fs::create_dir_all(&self.path)?;
        let mut s = String::new();
        File::open(self.path.join("ui.json"))?.read_to_string(&mut s)?;
        self.json = serde_json::from_str(s.as_str())?;
        Ok(())
    }

    fn save(&self) -> Result<(), Box<dyn Error>> {
        fs::create_dir_all(&self.path)?;
        let s = serde_json::to_string(&self.json)?;
        AtomicFile::new(self.path.join("ui.json"), AllowOverwrite).write(|f|
             f.write_all(s.as_bytes())
        )?;
        Ok(())
    }

    pub fn get_window(&self, window_id: &str) -> Option<&Window> {
        self.json.windows.get(window_id)
    }

    pub fn set_window(&mut self, window_id: &str, x: i32, y: i32, width: i32, height: i32) {
        self.json.windows.insert(window_id.to_owned(), Window { x, y, width, height });
        if let Err(err) = self.save() {
            log::error!("Error saving UiData: {}", err);
        }
    }
}
