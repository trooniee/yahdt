use std::path::{Path, PathBuf};

pub struct Config {
    path: PathBuf,
}

impl Config {
    pub fn from_dir(path: &Path) -> Self {
        Self {
            path: path.to_owned(),
        }
    }

    pub fn qml_override(&self) -> Option<PathBuf> {
        let mut path = self.path.clone();
        path.push("qml");
        if path.is_dir() {
            Some(path)
        } else {
            None
        }
    }
}
