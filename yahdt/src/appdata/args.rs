use docopt::Docopt;
use serde::Deserialize;
use std::path::PathBuf;

const USAGE: &'static str = r#"
Yet Another Hearthstone Deck Tracker.

Usage:
  hdt-rs [-v | --verbose] [--power-log=<powerlog>]
  hdt-rs (-h | --help)

Options:
  -h --help                    Show this screen.
  -v --verbose                 Trace.
  --power-log=<powerlog>       Override path to Power.log
"#;

#[derive(Deserialize)]
pub struct Args {
    pub flag_verbose: bool,
    pub flag_power_log: Option<PathBuf>,
}

impl Args {
    pub fn from_sys() -> Result<Self, docopt::Error> {
        Docopt::new(USAGE).unwrap().deserialize()
    }
}
