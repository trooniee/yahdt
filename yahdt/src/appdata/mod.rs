mod args;
mod config;
mod ui_data;
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;
use std::sync::RwLock;

pub struct AppData;

struct AppDataContent {
    args: args::Args,
    config: config::Config,
    ui_data: RwLock<ui_data::UiData>,
}

lazy_static::lazy_static! {
    static ref SINGLETON: AppDataContent = AppDataContent::open();
}

impl AppDataContent {
    fn open() -> Self {
        let args = args::Args::from_sys().unwrap_or_else(|e| e.exit());
        // TODO --config option to override config dir
        let level = if args.flag_verbose { log::Level::Trace }
        else { log::Level::Info };
        simple_logger::init_with_level(level).unwrap();

        let dirs = directories::ProjectDirs::from(
            "", "", env!("CARGO_PKG_NAME"),
            ).expect("ProjectDirs error");
        let config = config::Config::from_dir(dirs.config_dir());
        let ui_data = ui_data::UiData::from_dir(dirs.data_local_dir());

        Self { args, config, ui_data: RwLock::new(ui_data) }
    }
}

impl AppData {
    pub fn instantiate_singleton() {
        &*SINGLETON;
    }
    pub fn args() -> impl Deref<Target=args::Args> + 'static {
        &SINGLETON.args
    }
    pub fn config() -> impl Deref<Target=config::Config> + 'static {
        &SINGLETON.config
    }
    pub fn ui_data() -> impl Deref<Target=ui_data::UiData> + 'static {
        SINGLETON.ui_data.read().unwrap()
    }
    pub fn ui_data_mut() -> impl DerefMut<Target=ui_data::UiData> + 'static {
        SINGLETON.ui_data.write().unwrap()
    }

    pub fn power_log() -> PathBuf {
        Self::args().flag_power_log.clone().unwrap_or_else(|| PathBuf::from(PathBuf::from("/storage/GamesWine/BattleNet/Hearthstone/Logs/Power.log")))
    }
}
