use trackers::{CARDS_DB, CARDS_ID, CardInfo, DeckList};
use hearthstone_deckstring::Deck;
use std::convert::TryFrom;

#[derive(Default, Clone)]
pub struct DeckVec {
    vec: Vec<DeckVecItem>,
}

#[derive(Clone)]
pub struct DeckVecItem {
    pub card: CardInfo,
    pub amount: i64,
}

pub trait CardInfoMethods {
    fn as_str(&self) -> &str;
    fn mana_cost(&self) -> Option<i32>;
}

impl CardInfoMethods for CardInfo {
    fn as_str(&self) -> &str {
        match self {
            CardInfo::Unknown => "<unknown>",
            CardInfo::Revealed(card_id) => match CARDS_ID.get(card_id) {
                Some(card) => card.name.as_str(),
                None => {
                    log::warn!("Invalid CardID: {:?}", card_id);
                    card_id
                }
            },
        }
    }

    fn mana_cost(&self) -> Option<i32> {
        match self {
            CardInfo::Revealed(card_id) => CARDS_ID.get(card_id)?.mana_cost,
            _ => None,
        }
    }
}

impl From<DeckList> for DeckVec {
    fn from(source: DeckList) -> Self {
        let mut vec: Vec<DeckVecItem> = source.map.into_iter().map(|(card, amount)| DeckVecItem{card, amount}).collect();
        vec.sort_unstable_by(|a, b| (a.card.mana_cost(), a.card.as_str()).cmp(&(b.card.mana_cost(), b.card.as_str())));
        DeckVec { vec }
    }
}

impl DeckVec {
    pub fn len(&self) -> usize {
        self.vec.len()
    }

    pub fn get(&self, index: usize) -> Option<&DeckVecItem> {
        self.vec.get(index)
    }

    #[allow(dead_code)]
    pub fn print(&self) {
        for rec in self.vec.iter() {
            let mana_cost = match &rec.card {
                CardInfo::Revealed(card_id) => {
                    match CARDS_ID.get(&card_id) {
                        Some(rec) => match rec.mana_cost {
                            None => String::new(),
                            Some(m) => m.to_string(),
                        }
                        None => String::new(),
                    }
                }
                _ => String::new(),
            };
            println!("{:2}x <{}> {}", rec.amount, mana_cost, rec.card.as_str());
        }
    }
}

pub trait DeckListMethods {
    fn from_deck(deck: &Deck) -> Self;
}

impl DeckListMethods for DeckList {
    fn from_deck(deck: &Deck) -> Self {
        // TODO maybe copy hero card too?
        let mut deck_list = Self::new();
        for (card_db_id, amount) in deck.cards.iter() {
            match CARDS_DB.get(&card_db_id) {
                None => log::error!("Unknown DBF_ID in deck string: {}", card_db_id),
                Some(rec) => {
                    deck_list.insert_amount(
                        CardInfo::Revealed(rec.card_id.clone()),
                        i64::try_from(*amount).unwrap(),
                    );
                }
            }
        }
        deck_list
    }
}
