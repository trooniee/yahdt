mod appdata;
mod deck_list;
mod gamewatch;
mod qt;
pub use appdata::AppData;
pub use trackers::DeckList;

use std::thread;

fn main() -> Result<(), std::io::Error> {
    AppData::instantiate_singleton();

    // preevaluate cards db
    let _ = *trackers::CARDS_ID;
    let _ = *trackers::CARDS_DB;

    let mut watch = gamewatch::GameWatch::new(
        AppData::power_log()
    );

    let _tailf = thread::spawn(move || watch.exec());
    // TODO handle errors and panics in _tailf thread through UI

    qt::Loop::init_static();
    let mut qt = qt::Loop::new();
    qt.load_file("OurDeck.qml");
    qt.load_file("TheirDeck.qml");
    qt.load_file("SystemTray.qml");
    qt.exec();

    Ok(()) // soft exit from `tailf`
}
