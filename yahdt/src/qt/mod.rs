mod deck_model;
mod save_window_pos;
use crate::AppData;
pub use deck_model::send_track_results;
use qmetaobject::*;

pub struct Loop {
    engine: QmlEngine,
}

impl Loop {
    pub fn init_static() {
        match std::env::var_os("RESOURCE_NAME") {
            Some(ref s) if !s.is_empty() => {}
            _ => std::env::set_var("RESOURCE_NAME", env!("CARGO_PKG_NAME")),
        }
        my_resource();
        deck_model::register();
        save_window_pos::register();
    }

    pub fn new() -> Self {
        Self {
            engine: QmlEngine::new(),
        }
    }

    pub fn load_file(&mut self, uri: &str) {
        let mut prefix = String::from("qrc:/qml/") + uri;
        if let Some(path) = AppData::config().qml_override() {
            if let Ok(path) = path.join(uri).as_os_str().to_owned().into_string() {
                prefix = path;
            } else {
                log::error!("Cannot read qmls from unfriendly path: {}", path.display());
            }
        }
        self.engine.load_file(prefix.into());
    }

    pub fn exec(&mut self) {
        self.engine.exec()
    }
}

qrc!(my_resource,
     "qml" {
         "qml/CardTile.qml" as "CardTile.qml",
         "qml/DeckList.qml" as "DeckList.qml",
         "qml/OurDeck.qml" as "OurDeck.qml",
         "qml/SaveWindowPos.qml" as "SaveWindowPos.qml",
         "qml/SystemTray.qml" as "SystemTray.qml",
         "qml/TheirDeck.qml" as "TheirDeck.qml",
         "qml/icon128.png" as "icon128.png",
     }
);
