use clipboard::{ClipboardContext, ClipboardProvider};
use crate::deck_list::{CardInfoMethods, DeckListMethods, DeckVec, DeckVecItem};
use crate::gamewatch::TrackResults;
use cstr::*;
use lazy_static::lazy_static;
use qmetaobject::*;
use std::collections::HashMap;
use std::convert::{TryFrom, TryInto};
use std::sync::{Arc, Mutex};
use trackers::DeckList;

pub fn register() {
    qml_register_type::<DeckModel>(cstr!("yahdt"), 1, 0, cstr!("DeckModel"));
}

lazy_static! {
    /// list of callbacks to update deck models
    static ref UPDATE_DECK_MODELS: Mutex<Vec<Box<dyn Fn(Arc<TrackResults>) + Sync + Send>>> = Mutex::new(Vec::new());
    static ref LAST_TRACK_RESULTS: Mutex<Arc<TrackResults>> = Default::default();
}

pub fn send_track_results(results: TrackResults) {
    let results = Arc::new(results);
    let vec = UPDATE_DECK_MODELS.lock().unwrap();
    for f in vec.iter() {
        f(results.clone());
    }
    *LAST_TRACK_RESULTS.lock().unwrap() = results;
}

#[derive(Default, QObject)]
struct DeckModel {
    base: qt_base_class!(trait QAbstractListModel),
    /// is update handler installed
    installed: bool,
    deck_string: Option<(String, DeckList)>,
    /// decklist id ("our_natives", etc)
    decklist_id: qt_property!(QString; WRITE set_decklist_id),
    deck_vec: DeckVec,
    length: qt_property!(i32; READ row_count NOTIFY length_changed),
    length_changed: qt_signal!(),
    reset_deck_string: qt_method!(
    fn reset_deck_string(&mut self) {
        self.deck_string = None;
        self.reload_data();
    }),
    try_set_deck_string: qt_method!(
    fn try_set_deck_string(&mut self, maybe_deck_string: String) -> bool {
        if let Ok(deck) = hearthstone_deckstring::Deck::try_from(maybe_deck_string.as_str()) {
            self.deck_string = Some((maybe_deck_string, DeckList::from_deck(&deck)));
            self.reload_data();
            true
        } else {
            false
        }
    }),
    try_set_deck_string_from_clipboard: qt_method!(
    fn try_set_deck_string_from_clipboard(&mut self) -> bool {
        let mut ctx: ClipboardContext = match ClipboardProvider::new() {
            Err(ref error) => {
                log::error!("Cannot access clipboard context: {}", error.as_ref());
                // TODO error message in gui
                return false;
            }
            Ok(ctx) => ctx,
        };
        match ctx.get_contents() {
            Err(_) => false, // TODO error message in gui
            Ok(string) => self.try_set_deck_string(string),
        }
    }),
}

impl DeckModel {
    fn reload(&mut self) {
        // TODO `reset` resets scrolling position.  maybe replace it with something softer
        (self as &mut dyn QAbstractListModel).begin_reset_model();
        (self as &mut dyn QAbstractListModel).end_reset_model();
        self.length_changed();
    }

    fn set_decklist_id(&mut self, val: QString) {
        self.decklist_id = val;
        if !self.installed {
            self.installed = true;
            let qptr: QPointer<DeckModel> = QPointer::from(&*self);
            let func = queued_callback(move |results: Arc<TrackResults>| {
                qptr.as_pinned().map(|self_| {
                    self_.borrow_mut().update_data(results);
                });
            });
            let func = Box::new(func);
            // make sure we're up-to-date
            self.reload_data();
            UPDATE_DECK_MODELS.lock().unwrap().push(func);
        } else {
            self.reload();
        }
    }

    fn reload_data(&mut self) {
        self.update_data(LAST_TRACK_RESULTS.lock().unwrap().clone());
    }

    fn update_data(&mut self, results: Arc<TrackResults>) {
        let mut deck_list = match self.decklist_id.to_string().as_str() {
            "our_natives" => &results.our_natives,
            "our_foreigns" => &results.our_foreigns,
            "their_natives" => &results.their_natives,
            "their_deck_foreigns" => &results.their_deck_foreigns,
            "their_hand_foreigns" => &results.their_hand_foreigns,
            _ => {
                log::error!("Unknown decklist id: {:?}", self.decklist_id);
                return;
            }
        }.clone();
        if let Some((_, ref deck)) = self.deck_string {
            deck_list = deck - deck_list;
        }
        self.deck_vec = deck_list.into();
        self.reload();
    }
}

impl QAbstractListModel for DeckModel {
    fn row_count(&self) -> i32 {
        self.deck_vec.len().try_into().unwrap()
    }
    fn data(&self, index: QModelIndex, role: i32) -> QVariant {
        let index = index.row() as usize;
        match self.deck_vec.get(index) {
            None => QVariant::default(),
            Some(DeckVecItem { card, amount }) => {
                if role == USER_ROLE + 0 {
                    QString::from(card.as_str()).into()
                } else if role == USER_ROLE + 1 {
                    i32::try_from(*amount).unwrap().into()
                } else if role == USER_ROLE + 2 {
                    match card.mana_cost() {
                        None => QString::from("").into(),
                        Some(cost) => cost.into(),
                    }
                } else {
                    QVariant::default()
                }
            }
        }
    }
    fn role_names(&self) -> HashMap<i32, QByteArray> {
        let mut map = HashMap::new();
        map.insert(USER_ROLE + 0, "name".into());
        map.insert(USER_ROLE + 1, "amount".into());
        map.insert(USER_ROLE + 2, "mana_cost".into());
        map
    }
}
