use crate::AppData;
use cstr::*;
use qmetaobject::*;

pub fn register() {
    qml_register_type::<SaveWindowPosAccessor>(cstr!("yahdt"), 1, 0, cstr!("SaveWindowPosAccessor"));
}

#[derive(Default, QObject)]
struct SaveWindowPosAccessor {
    base: qt_base_class!(trait QObject),
    window_id: qt_property!(String),
    data_available: qt_property!(bool; READ data_available),
    x: qt_property!(i32; READ x CONST),
    y: qt_property!(i32; READ y CONST),
    width: qt_property!(i32; READ width CONST),
    height: qt_property!(i32; READ height CONST),
    save_data: qt_method!(fn save_data(&mut self, x: i32, y: i32, width: i32, height: i32) {
        AppData::ui_data_mut().set_window(self.window_id.as_ref(), x, y, width, height);
    }),
    // TODO remember screen with screen.serialNumber?
}

impl SaveWindowPosAccessor {
    fn data_available(&self) -> bool {
        AppData::ui_data().get_window(self.window_id.as_ref()).is_some()
    }
    fn x(&self) -> i32 {
        // those defaults are never supposed to hit. could as well be unwrap()
        AppData::ui_data().get_window(self.window_id.as_ref()).map_or(0, |data| data.x)
    }
    fn y(&self) -> i32 {
        AppData::ui_data().get_window(self.window_id.as_ref()).map_or(0, |data| data.y)
    }
    fn width(&self) -> i32 {
        AppData::ui_data().get_window(self.window_id.as_ref()).map_or(0, |data| data.width)
    }
    fn height(&self) -> i32 {
        AppData::ui_data().get_window(self.window_id.as_ref()).map_or(0, |data| data.height)
    }
}

