import QtQuick 2.6
import QtQuick.Window 2.6
import QtQuick.Layouts 1.3
import yahdt 1.0

Item {
	property alias model: list_view.model
	property int spacing: 2

	Layout.margins: 2
	Layout.fillWidth: true
	Layout.preferredHeight: (30 + spacing) * model.length - spacing
	ListView {
		height: parent.height
		width: parent.width
		id: list_view
		boundsBehavior: Flickable.StopAtBounds
		spacing: parent.spacing
		delegate: CardTile {}
	}
}
