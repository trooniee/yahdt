import QtQuick 2.6
import QtQuick.Window 2.6
import QtQuick.Layouts 1.3
import yahdt 1.0

Window {
	id: w_their_deck
	onClosing: close.accepted = false
	title: "Opponent - yahdt"
	width: 256
	height: screen.height * 0.75
	visible: true

	SaveWindowPos {
		window_id: "their_deck"
		wnd: w_their_deck
	}

	ColumnLayout {
		width: parent.width
		height: parent.height
		DeckList {
			model: DeckModel {
				decklist_id: "their_natives"
			}
		}
		Text {
			visible: their_hand_foreigns.length > 0
			font.italic: true
			text: "- foreigns/hand: -"
			Layout.alignment: Qt.AlignHCenter
		}
		DeckList {
			model: DeckModel {
				id: their_hand_foreigns
				decklist_id: "their_hand_foreigns"
			}
		}
		Text {
			visible: their_deck_foreigns.length > 0
			font.italic: true
			text: "- foreigns/deck: -"
			Layout.alignment: Qt.AlignHCenter
		}
		DeckList {
			model: DeckModel {
				id: their_deck_foreigns
				decklist_id: "their_deck_foreigns"
			}
		}
		Item {
			// fill the rest of the space
			Layout.fillHeight: true
		}
	}
}
