import QtQuick 2.6
import QtQuick.Window 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 1.4
import yahdt 1.0

Window {
	id: w_our_deck
	onClosing: close.accepted = false
	title: "Player - yahdt"
	width: 256
	height: screen.height * 0.75
	visible: true

	SaveWindowPos {
		window_id: "our_deck"
		wnd: w_our_deck
	}

	MouseArea {
		anchors.fill: parent
		acceptedButtons: Qt.RightButton
		onClicked: context_menu.popup()
	}

	Menu {
		id: context_menu
		MenuItem {
			text: qsTr("No Deck Mode")
			onTriggered: our_natives_model.reset_deck_string()
		}
		MenuItem {
			text: qsTr("Paste Deck Code from Clipboard")
			onTriggered: our_natives_model.try_set_deck_string_from_clipboard()
		}
	}

	ColumnLayout {
		anchors.fill: parent
		DeckList {
			model: DeckModel {
				id: our_natives_model
				decklist_id: "our_natives"
			}
		}
		Text {
			visible: our_foreigns.length > 0
			font.italic: true
			text: "- foreigns: -"
			Layout.alignment: Qt.AlignHCenter
		}
		DeckList {
			model: DeckModel {
				id: our_foreigns
				decklist_id: "our_foreigns"
			}
		}
		Item {
			// fill the rest of the space
			Layout.fillHeight: true
		}
	}
}
