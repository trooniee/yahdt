import QtQuick 2.6
import QtQuick.Window 2.6
import QtQuick.Layouts 1.3
import yahdt 1.0

Item {
	id: self
	property string window_id
	property QtObject wnd

	SaveWindowPosAccessor {
		id: acc
		window_id: self.window_id
	}

	Component.onCompleted: {
		if (acc.data_available) {
			wnd.x = acc.x
			wnd.y = acc.y
			wnd.width = acc.width
			wnd.height = acc.height
		}
	}
	Component.onDestruction: {
		acc.save_data(wnd.x, wnd.y, wnd.width, wnd.height)
	}
}
