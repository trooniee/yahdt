import QtQuick 2.6
import QtQuick.Window 2.6
import QtQuick.Layouts 1.3
import yahdt 1.0

Component {
	Rectangle {
		height: 30
		width: parent.width
		RowLayout {
			spacing: 2
			width: parent.width
			height: parent.height
			Rectangle {
				color: "darkblue"
				width: height
				height: parent.height
				Text {
					anchors.centerIn: parent
					color: "white"
					font.bold: true
					text: mana_cost
				}
			}
			Rectangle {
				color: "gray"
				Layout.fillWidth: true
				height: parent.height
				Text {
					x: 2
					height: parent.height
					verticalAlignment: Text.AlignVCenter
					font.strikeout: amount <= 0
					font.bold: amount > 0
					text: name
				}
			}
			Rectangle {
				visible: -1 >= amount || amount >= 2
				color: "darkorange"
				width: height
				height: parent.height
				Text {
					anchors.centerIn: parent
					color: "white"
					font.bold: true
					text: amount
				}
			}
		}
	}
}
