import Qt.labs.platform 1.0

SystemTrayIcon {
	iconSource: "icon128.png"
	visible: true
	tooltip: ["Ok Ok", "LOL"][Math.floor(Math.random()*2)]
	menu: Menu {
		MenuItem {
			text: qsTr("Quit")
			onTriggered: Qt.quit()
		}
	}
}
