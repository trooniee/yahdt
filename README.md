# Yet Another Hearthstone Deck Tracker.

![yahdt](yahdt/qml/icon128.png "Yahdt")

~~Work in progress, but pretty much usable.~~ I'm quitting HS for now; feel free to pick this project up. It has been tested extensively on linux. It should (supposedly) work on windows and macos with no issues. `/trackers/src/card_data/` has a script to download latest card data from [hearthstonejson.com](hearthstonejson.com).

### Prerequisites:

* recent stable rust
* qt>=5.11

On debian Buster:

    # apt-get install qtbase5-dev qtdeclarative5-dev qt5-qmake qml-module-qtquick2 qml-module-qtquick-window2 qml-module-qtquick-layouts qml-module-qtquick-controls qml-module-qtquick-controls2 qml-module-qt-labs-platform

# Usage:

Try to build and run it with:

    $ cargo run --release -- --power-log=/path/to/Power.log

Change your deck with Player's deck list context menu.

`log.config` should be in place. See https://github.com/HearthSim/Hearthstone-Deck-Tracker/wiki/Setting-up-the-log.config

If you're doing it on windows or mac, please report your success or failure in issue tracker.
