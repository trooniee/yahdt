use parsegame::ctxfree::*;

#[test]
fn test_parse_line() {
    assert_eq!(
        parse_line("D 20:22:55.0966490 GameState.DebugPrintPower() - CREATE_GAME"),
        (0, ParsedLine::CreateGame)
    );
    assert_eq!(
        parse_line("D 20:22:55.0966580 GameState.DebugPrintPower() -     GameEntity EntityID=1"),
        (4, ParsedLine::GameEntity { entity_id: 1 })
    );
    assert_eq!(
        parse_line("D 20:22:55.0966950 GameState.DebugPrintPower() -         tag=ZONE value=PLAY"),
        (8, ParsedLine::TagValue("ZONE", "PLAY"))
    );
    assert_eq!(
        parse_line(
            "D 20:22:55.0967340 GameState.DebugPrintPower() -     Player \
             EntityID=2 PlayerID=1 GameAccountId=[hi=144115198130930503 lo=17909181]"
        ),
        (
            4,
            ParsedLine::Player {
                entity_id: 2,
                player_id: 1,
                game_account_id: ("144115198130930503", "17909181"),
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:23:48.4756030 GameState.DebugPrintPower() -         \
             FULL_ENTITY - Creating ID=80 CardID=EX1_506a"
        ),
        (
            8,
            ParsedLine::FullEntity {
                entity_id: 80,
                card_id: Some("EX1_506a"),
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:10:21.2631360 GameState.DebugPrintPower() - TAG_CHANGE Entity=1 \
             tag=STATE value=RUNNING ", // trailing space is intentional
        ),
        (
            0,
            ParsedLine::TagChange {
                entity: SomewhatEntityId::Id(1),
                tag: "STATE",
                value: "RUNNING",
                metadata: TagChangeMetadata::None,
            }
        )
    );
    assert_eq!(
        parse_line("D 08:43:06.8453470 GameState.DebugPrintPower() -         TAG_CHANGE Entity=94 tag=DISPLAYED_CREATOR value=37 DEF CHANGE"),
        (8, ParsedLine::TagChange {
            entity: SomewhatEntityId::Id(94),
            tag: "DISPLAYED_CREATOR",
            value: "37",
            metadata: TagChangeMetadata::DefChange,
    }));
    assert_eq!(parse_line(
        "D 15:44:54.5904830 GameState.DebugPrintPower() - TAG_CHANGE Entity=[entityName=Drustvar Horror id=141 zone=SETASIDE zonePos=0 cardId=DAL_431t player=2] tag=ADDITIONAL_PLAY_REQS_1 value=54160 DEF CHANGE"),
        (0, ParsedLine::TagChange {
            entity: SomewhatEntityId::Id(141),
            tag: "ADDITIONAL_PLAY_REQS_1",
            value: "54160",
            metadata: TagChangeMetadata::DefChange,
    }));
    assert_eq!(
        parse_line(
            "D 20:10:21.2636650 GameState.DebugPrintPower() -     \
             SHOW_ENTITY - Updating Entity=29 CardID=EX1_507",
        ),
        (
            4,
            ParsedLine::ShowEntity {
                entity_id: 29,
                card_id: Some("EX1_507"),
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:10:56.2802700 GameState.DebugPrintPower() -     SHOW_ENTITY - Updating Entity\
             =[entityName=UNKNOWN ENTITY [cardType=INVALID] id=13 zone=DECK zonePos=0 cardId= \
             player=1] CardID=CS2_046"
        ),
        (
            4,
            ParsedLine::ShowEntity {
                entity_id: 13,
                card_id: Some("CS2_046"),
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:18:21.0782690 GameState.DebugPrintPower() -             HIDE_ENTITY - \
             Entity=151 tag=ZONE value=PLAY",
        ),
        (
            12,
            ParsedLine::HideEntity {
                entity_id: 151,
                tag: "ZONE",
                value: "PLAY",
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:21:00.6499510 GameState.DebugPrintPower() -     HIDE_ENTITY - \
             Entity=[entityName=Hounded! id=170 zone=PLAY zonePos=0 cardId=GIL_650e \
             player=1] tag=ZONE value=PLAY"
        ),
        (
            4,
            ParsedLine::HideEntity {
                entity_id: 170,
                tag: "ZONE",
                value: "PLAY",
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:21:47.6383930 GameState.DebugPrintPower() -                 \
             CHANGE_ENTITY - Updating Entity=192 CardID=GIL_547"
        ),
        (
            16,
            ParsedLine::ChangeEntity {
                entity_id: 192,
                card_id: Some("GIL_547")
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 20:21:47.6363440 GameState.DebugPrintPower() -                 \
             CHANGE_ENTITY - Updating Entity=[entityName=Zentimo id=143 zone=PLAY \
             zonePos=1 cardId=TRL_085 player=1] CardID=BOT_243"
        ),
        (
            16,
            ParsedLine::ChangeEntity {
                entity_id: 143,
                card_id: Some("BOT_243")
            }
        )
    );
    assert_eq!(
        parse_line(
            "D 07:43:45.2459267 GameState.DebugPrintPower() -     TAG_CHANGE \
             Entity=[entityName=Replicating Menace id=6 zone=HAND zonePos=1 \
             cardId=BOT_312 player=1] tag=1196 value=1"
        ),
        (
            4,
            ParsedLine::TagChange {
                entity: SomewhatEntityId::Id(6),
                tag: "1196",
                value: "1",
                metadata: TagChangeMetadata::None,
            }
        )
    );
    assert_eq!(parse_line(
        "D 20:10:21.3437410 GameState.DebugPrintGame() - PlayerID=2, PlayerName=UNKNOWN HUMAN PLAYER"),
        (0, ParsedLine::PlayerData {
            entity_id: 2,
            name: "UNKNOWN HUMAN PLAYER",
        })
    );
}

#[test]
fn test_extract_somewhat_entity_id() {
    use lazy_static::lazy_static;
    use regex::Regex;
    lazy_static! {
        static ref UNIT: Regex = Regex::new(r"(.*)$").unwrap();
    }
    assert_eq!(
        UNIT.captures("13").unwrap().extract_somewhat_entity_id(1),
        SomewhatEntityId::Id(13)
    );
    assert_eq!(
        UNIT.captures("GameEntity")
            .unwrap()
            .extract_somewhat_entity_id(1),
        SomewhatEntityId::Game
    );
    assert_eq!(
        UNIT.captures("Player#8471")
            .unwrap()
            .extract_somewhat_entity_id(1),
        SomewhatEntityId::PlayerName("Player#8471")
    );
    assert_eq!(
        UNIT.captures("UNKNOWN HUMAN PLAYER")
            .unwrap()
            .extract_somewhat_entity_id(1),
        SomewhatEntityId::UnknownHumanPlayer
    );
    assert_eq!(
        UNIT.captures("The Innkeeper")
            .unwrap()
            .extract_somewhat_entity_id(1),
        SomewhatEntityId::TheInnkeeper
    );
}
