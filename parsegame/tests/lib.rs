#![allow(non_snake_case)]
use parsegame::*;

macro_rules! input {
    ($p:expr,$i:expr) => {
        assert!(!$p.has_output());
        $p.feed_line($i);
    };
}
macro_rules! output {
    ($p:expr,$pat:pat$(,$($c:expr)?)?) => {
        match $p.pop() {
            None => panic!("No output"),
            Some(inner) => match inner {
                $pat => {
                    $($(assert!($c))?)?
                }
                inner => panic!("Match failed: found {:?}", inner),
            },
        }
    };
}
macro_rules! pause {
    ($p:expr) => {
        assert!(!$p.has_output());
        $p.feed_pause();
    };
}

#[test]
fn test_sequence() {
    let mut parser = Parser::new();
    assert_eq!(false, parser.is_holding_input());
    input!(
        parser,
        "D 20:10:21.2596860 GameState.DebugPrintPower() - FULL_ENTITY - Creating ID=4 CardID=\r\n"
    );
    assert_eq!(true, parser.is_holding_input());
    input!(
        parser,
        "D 20:10:21.2597090 GameState.DebugPrintPower() -     tag=ZONE value=DECK\r\n"
    );
    assert_eq!(true, parser.is_holding_input());
    input!(
        parser,
        "D 20:10:21.2597380 GameState.DebugPrintPower() - FULL_ENTITY - Creating ID=5 CardID=\r\n"
    );
    output!(parser, GameEvent::EntityUpdate {
        entity_id: 4,
        card_id: Some(None),
        ref tags,
        metadata: EntityUpdateMetadata::FullEntityCreating { .. },
        ..
    }, tags == &Tags::single(String::from("ZONE"), String::from("DECK")));
    assert_eq!(true, parser.is_holding_input());
    pause!(parser);
    output!(parser, GameEvent::EntityUpdate {
        entity_id: 5,
        card_id: Some(None),
        ref tags,
        metadata: EntityUpdateMetadata::FullEntityCreating { .. },
        ..
    }, tags == &Tags::new());
    assert_eq!(false, parser.is_holding_input());
    assert!(!parser.has_output());
}

#[test]
fn test_GameEntity() {
    let mut parser = Parser::new();
    input!(parser,
        "D 14:48:03.2138440 GameState.DebugPrintPower() -     GameEntity EntityID=1\r\n");
    pause!(parser);
    output!(parser, GameEvent::EntityUpdate {
        entity_id: 1,
        ..
    });
    input!(parser,
        "D 14:48:03.4350860 GameState.DebugPrintPower() - TAG_CHANGE Entity=GameEntity tag=STEP value=BEGIN_MULLIGAN \r\n");
    output!(parser, GameEvent::EntityUpdate {
        entity_id: 1,
        ..
    });

}
