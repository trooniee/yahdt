use super::types::*;
use std::collections::HashMap;
use std::str::FromStr;

#[derive(Debug, PartialEq)]
pub struct Tags {
    map: HashMap<String, String>,
}

macro_rules! tag_parse {
    ($tag:ident, $out:ty $(,)?) => {
        #[allow(non_snake_case)]
        pub fn $tag(&self) -> Option<$out> {
            self.get(stringify!($tag)).and_then(|value| match value.parse() {
                Ok(some) => Some(some),
                Err(err) => {
                    // TODO Possibility of empty string should sometimes be handled as
                    // Option<Option<_>>
                    log::warn!("Failed to parse {} {:?}: {:?}",
                                stringify!($tag), value, err);
                    None
                }
            })
        }
    }
}

impl Tags {
    pub fn new() -> Self {
        Self {
            map: HashMap::new(),
        }
    }

    pub fn single(tag: String, value: String) -> Self {
        let mut tags = Self::new();
        tags.insert(tag, value);
        tags
    }

    /// Returns old value if it was present... for now. may instead want to check for duplication
    pub fn insert(&mut self, tag: String, value: String) -> Option<String> {
        self.map.insert(tag, value)
    }

    pub fn get(&self, k: &str) -> Option<&String> {
        self.map.get(k)
    }

    tag_parse!(CREATOR_DBID, CardDbId);
    tag_parse!(CONTROLLER, PlayerId);
    tag_parse!(ZONE, Zone);
    tag_parse!(NEXT_STEP, GameStep);
    tag_parse!(STEP, GameStep);
    tag_parse!(STATE, GameState);
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum Zone {
    Play,
    Hand,
    Deck,
    Graveyard,
    SetAside,
    RemovedFromGame,
    Secret,
}

impl FromStr for Zone {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "PLAY" => Zone::Play,
            "HAND" => Zone::Hand,
            "DECK" => Zone::Deck,
            "GRAVEYARD" => Zone::Graveyard,
            "SETASIDE" => Zone::SetAside,
            "REMOVEDFROMGAME" => Zone::RemovedFromGame,
            "SECRET" => Zone::Secret,
            _ => return Err(()),
        })
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum GameStep {
    BeginMulligan,
    MainReady,
    MainStartTriggers,
    MainStart,
    MainAction,
    MainEnd,
    MainCleanup,
    MainNext,
    FinalWrapUp,
    FinalGameOver,
}

impl FromStr for GameStep {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "BEGIN_MULLIGAN" => GameStep::BeginMulligan,
            "MAIN_READY" => GameStep::MainReady,
            "MAIN_START_TRIGGERS" => GameStep::MainStartTriggers,
            "MAIN_START" => GameStep::MainStart,
            "MAIN_ACTION" => GameStep::MainAction,
            "MAIN_END" => GameStep::MainEnd,
            "MAIN_CLEANUP" => GameStep::MainCleanup,
            "MAIN_NEXT" => GameStep::MainNext,
            "FINAL_WRAPUP" => GameStep::FinalWrapUp,
            "FINAL_GAMEOVER" => GameStep::FinalGameOver,
            _ => return Err(()),
        })
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
pub enum GameState {
    Running,
    Complete,
}

impl FromStr for GameState {
    type Err = ();
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(match s {
            "RUNNING" => GameState::Running,
            "COMPLETE" => GameState::Complete,
            _ => return Err(()),
        })
    }
}
