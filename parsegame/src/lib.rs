pub mod ctxfree;
mod re;
pub mod tags;
pub mod types;

use ctxfree::{parse_line, ParsedLine, SomewhatEntityId, TagChangeMetadata};
use std::collections::VecDeque;
pub use tags::Tags;
pub use types::*;

#[derive(Debug, PartialEq)]
pub enum EntityUpdateMetadata {
    GameEntity,
    Player {
        player_id: PlayerId,
        game_account_id: GameAccountId,
    },
    FullEntityCreating,
    TagChange {
        def_change: bool,
    },
    ShowEntityUpdating,
    HideEntity,
    ChangeEntityUpdating,
}

#[derive(Debug)]
pub enum GameEvent {
    /// New game is being started
    CreateGame,
    EntityUpdate {
        entity_id: EntityId,
        /// None: not changing; Some(None): (possibly) changing to None; Some(Some(card_id)):
        /// (possibly) changing to card_id
        card_id: Option<Option<String>>,
        tags: Tags,
        metadata: EntityUpdateMetadata,
    },
    SomewhatEntityUpdate {
        somewhat_entity_id: (),
        card_id: Option<Option<String>>,
        tags: Tags,
        metadata: EntityUpdateMetadata,
    },
    /// End of input stream: Power.log truncated, probably due to HS client restart
    EOF,
}

impl<'a> From<SomewhatEntityId<'a>> for String {
    fn from(e: SomewhatEntityId) -> String {
        match e {
            SomewhatEntityId::Id(id) => id.to_string(),
            SomewhatEntityId::Game => "GameEntity".into(),
            SomewhatEntityId::PlayerName(p) => p.to_owned(),
            SomewhatEntityId::UnknownHumanPlayer => "UNKNOWN HUMAN PLAYER".into(),
            SomewhatEntityId::TheInnkeeper => "The Innkeeper".into(),
            SomewhatEntityId::Other(s) => s.to_owned(),
        }
    }
}

impl GameEvent {
    pub fn tags_ref(&self) -> Option<&Tags> {
        match self {
            GameEvent::EntityUpdate { ref tags, .. } => Some(tags),
            GameEvent::SomewhatEntityUpdate { ref tags, .. } => Some(tags),
            GameEvent::CreateGame | GameEvent::EOF => None,
        }
    }

    pub fn tags_mut(&mut self) -> Option<&mut Tags> {
        match self {
            GameEvent::EntityUpdate { ref mut tags, .. } => Some(tags),
            GameEvent::SomewhatEntityUpdate { ref mut tags, .. } => Some(tags),
            GameEvent::CreateGame | GameEvent::EOF => None,
        }
    }
}

pub struct Parser {
    line_number: usize,
    output: Option<GameEvent>,
    output_q: VecDeque<GameEvent>,
    context: GameContext,
}

#[derive(Default)]
struct GameContext {
    game_entity: Option<EntityId>,
    /// 0 - unused; 1, 2 - player 1&2 names if known (i.e. anything but UnknownHumanPlayer)
    players: [Option<String>; 3],
}

impl Parser {
    pub fn new() -> Self {
        Self {
            line_number: 0,
            output: None,
            output_q: VecDeque::with_capacity(5),
            context: GameContext::default(),
        }
    }

    pub fn reset_line_number(&mut self) {
        self.line_number = 0;
    }

    pub fn feed_line<'a>(&'_ mut self, line: &'a str) -> bool {
        let input_line = line;
        if self.has_output() {
            log::warn!("output pending: consider processing it first");
        }
        self.line_number += 1;
        if !line.ends_with('\n') {
            log::warn!(
                "line #{} doesn't end with '\\n': {:?}",
                self.line_number,
                input_line
            );
        }
        let line = line.trim_end();
        let (_indent, line) = parse_line(line);
        match line {
            // GameState.DebugPrintPower:
            ParsedLine::Other(_) => {
                self.push_current();
            }
            ParsedLine::CreateGame => {
                self.push_current();
                self.output = Some(GameEvent::CreateGame);
                self.push_current();
                self.context = GameContext::default();
            }
            ParsedLine::GameEntity { entity_id } => {
                self.push_current();
                self.context.game_entity = Some(entity_id);
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: Some(None),
                    tags: Tags::new(),
                    metadata: EntityUpdateMetadata::GameEntity,
                });
            }
            ParsedLine::TagValue(tag, value) => {
                if let Some(ref mut current) = self.output {
                    if let Some(ref mut tags) = current.tags_mut() {
                        tags.insert(String::from(tag), String::from(value));
                    } else {
                        log::error!(
                            "line #{}: unexpected TagValue: {:?}",
                            self.line_number,
                            input_line
                        );
                    }
                } else {
                    log::error!(
                        "line #{}: unexpected TagValue: {:?}",
                        self.line_number,
                        input_line
                    );
                }
            }
            ParsedLine::Player {
                entity_id,
                player_id,
                game_account_id,
            } => {
                self.push_current();
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: Some(None),
                    tags: Tags::new(),
                    metadata: EntityUpdateMetadata::Player {
                        player_id,
                        game_account_id: GameAccountId(
                            String::from(game_account_id.0),
                            String::from(game_account_id.1),
                        ),
                    },
                });
            }
            ParsedLine::FullEntity { entity_id, card_id } => {
                self.push_current();
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: Some(card_id.map(String::from)),
                    tags: Tags::new(),
                    metadata: EntityUpdateMetadata::FullEntityCreating,
                });
            }
            ParsedLine::TagChange { entity, tag, value, metadata } => {
                self.push_current();
                let tags = Tags::single(String::from(tag), String::from(value));
                let card_id = None;
                let metadata = EntityUpdateMetadata::TagChange {
                    def_change: metadata == TagChangeMetadata::DefChange,
                };

                debug_assert!(self.output.is_none());
                self.output = match entity {
                    SomewhatEntityId::Id(entity_id) => Some(GameEvent::EntityUpdate {
                        entity_id,
                        card_id,
                        tags,
                        metadata,
                    }),
                    SomewhatEntityId::Game => match self.context.game_entity {
                        Some(entity_id) => Some(GameEvent::EntityUpdate {
                            entity_id, card_id, tags, metadata,
                        }),
                        None => {
                            log::error!("SomewhatEntityId::Game encountered while GameEntity id is not identified");
                            None // TODO send failure marker event
                        }
                    }
                    SomewhatEntityId::UnknownHumanPlayer => {
                        let mut player = 0;
                        if self.context.players[1].is_some() {
                            if self.context.players[2].is_some() {
                                log::error!("SomewhatEntityId::UnknownHumanPlayer is unexpected");
                            } else {
                                player = 2;
                            }
                        } else {
                            if self.context.players[2].is_some() {
                                player = 1;
                            } else {
                                log::error!("SomewhatEntityId::UnknownHumanPlayer is ambiguous");
                            }
                        }
                        if player == 0 {
                             // TODO send failure marker event
                             None
                        } else {
                            Some(GameEvent::EntityUpdate { entity_id: player, card_id, tags, metadata })
                        }
                    }
                    somewhat_entity_id => {
                        let mut player = 0;
                        let name = String::from(somewhat_entity_id);
                        if Some(&name) == self.context.players[1].as_ref() {
                            if Some(&name) == self.context.players[2].as_ref() {
                                log::error!("SomewhatEntityId::PlayerId is ambiguous");
                            } else {
                                player = 1;
                            }
                        } else {
                            if Some(&name) == self.context.players[2].as_ref() {
                                player = 2;
                            } else {
                                if self.context.players[1].is_none() {
                                    if self.context.players[2].is_none() {
                                        log::error!("SomewhatEntityId::PlayerId is ambiguous (both are UNKNOWN HUMAN PLAYERS)");
                                    } else {
                                        player = 2;
                                    }
                                } else {
                                    if self.context.players[2].is_none() {
                                        player = 1;
                                    } else {
                                        log::error!("SomewhatEntityId::PlayerId is unexpected");
                                    }
                                }
                            }
                        }
                        if player == 0 {
                             // TODO send failure marker event
                            None
                        } else {
                            Some(GameEvent::EntityUpdate { entity_id: player, card_id, tags, metadata })
                        }
                    }
                };
                self.push_current();
            }
            ParsedLine::ShowEntity { entity_id, card_id } => {
                self.push_current();
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: Some(card_id.map(String::from)),
                    tags: Tags::new(),
                    metadata: EntityUpdateMetadata::ShowEntityUpdating,
                });
            }
            ParsedLine::HideEntity {
                entity_id,
                tag,
                value,
            } => {
                self.push_current();
                let tags = Tags::single(String::from(tag), String::from(value));
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: None,
                    tags,
                    metadata: EntityUpdateMetadata::HideEntity,
                });
                self.push_current();
            }
            ParsedLine::ChangeEntity { entity_id, card_id } => {
                self.push_current();
                self.output = Some(GameEvent::EntityUpdate {
                    entity_id,
                    card_id: Some(card_id.map(String::from)),
                    tags: Tags::new(),
                    metadata: EntityUpdateMetadata::ChangeEntityUpdating,
                });
            }
            ParsedLine::BlockStartPlaceholder
            | ParsedLine::BlockEnd
            | ParsedLine::MetadataPlaceholder
            | ParsedLine::InfoPlaceholder
            | ParsedLine::SubSpellStartPlaceholder
            | ParsedLine::SubSpellEnd => {
                self.push_current();
                // TODO complete these
            }
            // GameState.DebugPrintGame:
            ParsedLine::PlayerData { entity_id, name } => {
                debug_assert!(1 <= entity_id && entity_id <= 2);
                if name != "UNKNOWN HUMAN PLAYER" {
                    self.context.players[entity_id] = Some(name.to_owned());
                }
            }
        };
        self.has_output()
    }

    /// Notify parser about a pause in input stream
    ///
    /// Can be called multiple times
    pub fn feed_pause(&mut self) -> bool {
        self.push_current();
        self.has_output()
    }

    /// Notify parser about end of current stream
    pub fn feed_end(&mut self) -> bool {
        self.push_current();
        self.output_q.push_back(GameEvent::EOF);
        self.has_output()
    }

    /// Return True if parser holds unprocessed input lines
    ///
    /// If True, `feed_pause` will cause processing that input and possibly new data will appear
    pub fn is_holding_input(&self) -> bool {
        self.output.is_some()
    }

    fn push_current(&mut self) {
        if let Some(event) = self.output.take() {
            self.output_q.push_back(event);
        }
    }

    pub fn has_output(&self) -> bool {
        !self.output_q.is_empty()
    }

    pub fn pop(&mut self) -> Option<GameEvent> {
        self.output_q.pop_front()
    }
}
