//! Parse a line w/o access to any context

use super::re;
use super::types::{EntityId, PlayerId};

#[derive(Debug, PartialEq)]
pub enum ParsedLine<'a> {
    Other(&'a str),
    // GameState.DebugPrintPower:
    CreateGame,
    GameEntity {
        entity_id: EntityId,
    },
    TagValue(&'a str, &'a str),
    Player {
        entity_id: EntityId,
        player_id: PlayerId,
        game_account_id: (&'a str, &'a str),
    },
    FullEntity {
        entity_id: EntityId,
        card_id: Option<&'a str>,
    },
    TagChange {
        entity: SomewhatEntityId<'a>,
        tag: &'a str,
        value: &'a str,
        metadata: TagChangeMetadata,
    },
    ShowEntity {
        entity_id: EntityId,
        card_id: Option<&'a str>,
    },
    HideEntity {
        entity_id: EntityId,
        tag: &'a str,
        value: &'a str,
    },
    ChangeEntity {
        entity_id: EntityId,
        card_id: Option<&'a str>,
    },
    BlockStartPlaceholder,
    BlockEnd,
    MetadataPlaceholder,
    InfoPlaceholder,
    SubSpellStartPlaceholder,
    SubSpellEnd,
    // GameState.DebugPrintGame:
    PlayerData {
        entity_id: EntityId,
        name: &'a str
    },
}

#[derive(Debug, PartialEq)]
pub enum TagChangeMetadata {
    None,
    DefChange,
}

/// Poorly specified entity ID
#[derive(Debug, PartialEq, Eq)]
pub enum SomewhatEntityId<'a> {
    Id(EntityId),
    Game,
    PlayerName(&'a str),
    UnknownHumanPlayer,
    TheInnkeeper,
    Other(&'a str),
}

trait CapturesExtract<'t, T: 't> {
    /// Convert the match associated with the capture group at index `i` to `T`
    fn extract(&'_ self, i: usize) -> T;
}

impl<'t> CapturesExtract<'t, &'t str> for regex::Captures<'t> {
    fn extract(&'_ self, i: usize) -> &'t str {
        self.get(i).unwrap().as_str()
    }
}

impl<'t> CapturesExtract<'t, usize> for regex::Captures<'t> {
    fn extract(&'_ self, i: usize) -> usize {
        self.get(i).unwrap().as_str().parse().unwrap()
    }
}

pub trait CapturesExtractMetadata<'t, T: 't> {
    fn extract_metadata(&'_ self, i: usize) -> T;
}

impl <'t> CapturesExtractMetadata<'t, TagChangeMetadata> for regex::Captures<'t> {
    fn extract_metadata(&'_ self, i: usize) -> TagChangeMetadata {
        match self.get(i) {
            None => TagChangeMetadata::None,
            Some(ref c) if c.as_str() == "DEF CHANGE" => TagChangeMetadata::DefChange,
            _ => unreachable!(),
        }
    }
}

pub trait CapturesExtractTyped<'t> {
    /// Extract card id
    ///
    /// Convert the match associated with the capture group at index `i` to `Some(&str)`, or `None`
    /// if the string is empty
    fn extract_card_id(&'_ self, i: usize) -> Option<&'t str>;

    fn extract_somewhat_entity_id(&'_ self, i: usize) -> SomewhatEntityId<'t>;
}

impl<'t> CapturesExtractTyped<'t> for regex::Captures<'t> {
    fn extract_card_id(&'_ self, i: usize) -> Option<&'t str> {
        match self.get(i).unwrap().as_str() {
            "" => None,
            text => Some(text),
        }
    }

    fn extract_somewhat_entity_id(&'_ self, i: usize) -> SomewhatEntityId<'t> {
        let text: &str = self.extract(i);
        match re::SOMEWHAT_ENTITY_ID.captures(text) {
            None => SomewhatEntityId::Other(text),
            Some(caps) => {
                if let Some(id) = caps.name("id") {
                    SomewhatEntityId::Id(id.as_str().parse().unwrap())
                } else if let Some(player_name) = caps.name("player") {
                    SomewhatEntityId::PlayerName(player_name.as_str())
                } else if let Some(_) = caps.name("game") {
                    SomewhatEntityId::Game
                } else if let Some(_) = caps.name("unknown") {
                    SomewhatEntityId::UnknownHumanPlayer
                } else if let Some(_) = caps.name("innkeeper") {
                    SomewhatEntityId::TheInnkeeper
                } else {
                    SomewhatEntityId::Other(text)
                }
            }
        }
    }
}

pub fn parse_line<'a>(line: &'a str) -> (usize, ParsedLine<'a>) {
    debug_assert!(!line.ends_with("\n"));

    if let Some(cap) = re::GameState_DebugPrintPower_LOG.captures(line) {
        let indent: &str = cap.extract(1);
        let indent = indent.len();
        (indent, {
            let command = cap.extract(2);

            if let Some(_cap) = re::CREATE_GAME.captures(command) {
                ParsedLine::CreateGame
            } else if let Some(cap) = re::GameEntity.captures(command) {
                ParsedLine::GameEntity {
                    entity_id: cap.extract(1),
                }
            } else if let Some(cap) = re::tag_value.captures(command) {
                ParsedLine::TagValue(cap.extract(1), cap.extract(2))
            } else if let Some(cap) = re::Player.captures(command) {
                ParsedLine::Player {
                    entity_id: cap.extract(1),
                    player_id: cap.extract(2),
                    game_account_id: (cap.extract(3), cap.extract(4)),
                }
            } else if let Some(cap) = re::FULL_ENTITY_Creating.captures(command) {
                ParsedLine::FullEntity {
                    entity_id: cap.extract(1),
                    card_id: cap.extract_card_id(2),
                }
            } else if let Some(cap) = re::TAG_CHANGE_ID.captures(command) {
                ParsedLine::TagChange {
                    entity: cap.extract_somewhat_entity_id(1),
                    tag: cap.extract(2),
                    value: cap.extract(3),
                    metadata: cap.extract_metadata(4),
                }
            } else if let Some(cap) = re::TAG_CHANGE_DESC.captures(command) {
                ParsedLine::TagChange {
                    entity: SomewhatEntityId::Id(cap.extract(1)),
                    tag: cap.extract(2),
                    value: cap.extract(3),
                    metadata: cap.extract_metadata(4),
                }
            } else if let Some(cap) = re::SHOW_ENTITY_ID.captures(command) {
                ParsedLine::ShowEntity {
                    entity_id: cap.extract(1),
                    card_id: cap.extract_card_id(2),
                }
            } else if let Some(cap) = re::SHOW_ENTITY_DESC.captures(command) {
                ParsedLine::ShowEntity {
                    entity_id: cap.extract(1),
                    card_id: cap.extract_card_id(2),
                }
            } else if let Some(cap) = re::HIDE_ENTITY_ID.captures(command) {
                ParsedLine::HideEntity {
                    entity_id: cap.extract(1),
                    tag: cap.extract(2),
                    value: cap.extract(3),
                }
            } else if let Some(cap) = re::HIDE_ENTITY_DESC.captures(command) {
                ParsedLine::HideEntity {
                    entity_id: cap.extract(1),
                    tag: cap.extract(2),
                    value: cap.extract(3),
                }
            } else if let Some(cap) = re::CHANGE_ENTITY_ID.captures(command) {
                ParsedLine::ChangeEntity {
                    entity_id: cap.extract(1),
                    card_id: cap.extract_card_id(2),
                }
            } else if let Some(cap) = re::CHANGE_ENTITY_DESC.captures(command) {
                ParsedLine::ChangeEntity {
                    entity_id: cap.extract(1),
                    card_id: cap.extract_card_id(2),
                }
            } else if let Some(_cap) = re::BLOCK_START_PLACEHOLDER.captures(command) {
                ParsedLine::BlockStartPlaceholder
            } else if let Some(_cap) = re::BLOCK_END.captures(command) {
                ParsedLine::BlockEnd
            } else if let Some(_cap) = re::META_DATA_PLACEHOLDER.captures(command) {
                ParsedLine::MetadataPlaceholder
            } else if let Some(_cap) = re::INFO_PLACEHOLDER.captures(command) {
                ParsedLine::InfoPlaceholder
            } else if let Some(_cap) = re::SUB_SPELL_START.captures(command) {
                ParsedLine::SubSpellStartPlaceholder
            } else if let Some(_cap) = re::SUB_SPELL_END.captures(command) {
                ParsedLine::SubSpellEnd
            } else {
                log::error!("Skipping: {:?}", line);
                ParsedLine::Other(line) // indent may be important here to catch unknown lines breaking tag/value sequence
            }
        })
    } else if let Some(cap) = re::GameState_DebugPrintGame_LOG.captures(line) {
        (0, {
            let data: &str = cap.extract(1);
            if let Some(cap) = re::GameState_DebugPrintGame_PLAYER_DATA.captures(data) {
                ParsedLine::PlayerData {
                    entity_id: cap.extract(1),
                    name: cap.extract(2),
                }
            } else {
                log::debug!("Skipping: {:?}", line);
                ParsedLine::Other(line)
            }

        })
    } else {
        (0, ParsedLine::Other(line))
    }
}
