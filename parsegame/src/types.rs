// TODO use u32 where appropriate
pub type EntityId = usize;
pub type PlayerId = usize;
pub type CardId = String; // TODO replace with configurable string reference container
pub type CardDbId = u64;

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub struct GameAccountId(pub String, pub String);
