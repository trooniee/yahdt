//! Regular expressions used by `ctxfree` module

#![allow(non_upper_case_globals)]
use lazy_static::lazy_static;
use regex::Regex;

#[allow(unused_macros)]
macro_rules! defre_test {
    ($c:expr, $i:expr, None) => {
        assert_eq!($c.get($i), None);
    };
    ($c:expr, $i:expr, $m: literal) => {
        assert_eq!($c.get($i).unwrap().as_str(), $m);
    };
}

/// Define regular expression, and create test for it.
/// # Examples
///
/// ```
/// defre!(
///     re, // definition name
///     test_re, // test function name
///     r"foo ([a-z])([0-9])?$" => // regular expression. must end with '$'
///     r"foo x3", // example input: must be matched
///     "x", "3"; // expected captures. `;` signifies end of test
///     r"foo y", // second test case
///     "y", None; // None for no capture
///     );
/// ```
macro_rules! defre {
    ($i:ident, $t:ident, $r:expr => $($e:literal $(,$m:tt)* $(,)?);* $(;)?) => {
        lazy_static! {
            pub static ref $i: Regex = Regex::new($r).unwrap();
        }

        #[cfg(test)]
        #[allow(non_snake_case)]
        #[test]
        fn $t() {
            #![allow(unused_mut)]
            assert!($r.starts_with("^"));
            assert!($r.ends_with("$"));
            $(
            let _c = $i.captures($e).unwrap();
            let mut i = 1;
            $(
                defre_test!(_c, i, $m);
                i += 1;
            )*
            assert_eq!($i.captures_len(), i);
            )*
        }
    };
}

defre!(GameState_DebugPrintPower_LOG, test_GameState_DebugPrintPower_log,
r"^D \d\d:\d\d:\d\d(?:\.\d{7})? GameState\.DebugPrintPower\(\) - ( *)(.*)$"
=>
r"D 20:10:21.2590970 GameState.DebugPrintPower() -     Player EntityID=2 PlayerID=1 GameAccountId=[hi=144115198130930503 lo=20935447]",
"    ",
"Player EntityID=2 PlayerID=1 GameAccountId=[hi=144115198130930503 lo=20935447]";

"D 21:20:17 GameState.DebugPrintPower() -             tag=ATTACHED value=201",
"            ",
"tag=ATTACHED value=201";
);

defre!(
    CREATE_GAME,
    test_CREATE_GAME,
    r"^CREATE_GAME$" =>
    "CREATE_GAME",
);

defre!(
    GameEntity,
    test_GameEntity,
    r"^GameEntity EntityID=(\d+)$" =>
    r"GameEntity EntityID=1",
    r"1";
);

defre!(
    tag_value,
    test_tag_value,
    r"^tag=(\S+) value=(\S+)$" =>
    "tag=CREATOR_DBID value=1066",
    "CREATOR_DBID",
    "1066";
);

defre!(
    Player,
    test_Player,
    r"^Player EntityID=(\d+) PlayerID=(\d+) GameAccountId=\[hi=(\S+) lo=(\S+)]$" =>
    r"Player EntityID=3 PlayerID=2 GameAccountId=[hi=144115198130930503 lo=30992491]",
    "3",
    "2",
    "144115198130930503",
    "30992491";
);

defre!(
    FULL_ENTITY_Creating,
    test_FULL_ENTITY_Creating,
    r"^FULL_ENTITY - Creating ID=(\d+) CardID=([A-Z_0-9a-z]*)$" =>
    r"FULL_ENTITY - Creating ID=66 CardID=HERO_01",
    "66",
    "HERO_01";
);

lazy_static! {
    pub static ref SOMEWHAT_ENTITY_ID: Regex = Regex::new(
        r"^(?P<id>\d+)|(?P<game>GameEntity)|(?P<unknown>UNKNOWN HUMAN PLAYER)|(?P<innkeeper>The Innkeeper)|(?P<player>\S+#\d+)$"
    )
    .unwrap();
}
const SOMEWHAT_ENTITY_ID_INCL: &'static str =
    r"\d+|GameEntity|UNKNOWN HUMAN PLAYER|The Innkeeper|\S+#\d+";

#[cfg(test)]
fn test_somewhat_entity_id_example(example: &str, cap_name: &str, cap_result: &str) {
    lazy_static! {
        static ref incl: Regex =
            Regex::new(&("^".to_string() + &SOMEWHAT_ENTITY_ID_INCL + "$")).unwrap();
    }
    let match1 = SOMEWHAT_ENTITY_ID.captures(example).unwrap();
    let match2 = incl.captures(example).unwrap();
    assert_eq!(match1.get(0), match2.get(0)); // compare matches, not strings
    assert_eq!(match1.name(cap_name).unwrap().as_str(), cap_result);
}

#[cfg(test)]
#[allow(non_snake_case)]
#[test]
fn test_SOMEWHAT_ENTITY_ID() {
    assert!(SOMEWHAT_ENTITY_ID.as_str().starts_with("^"));
    assert!(SOMEWHAT_ENTITY_ID.as_str().ends_with("$"));
    test_somewhat_entity_id_example("45", "id", "45");
    test_somewhat_entity_id_example("GameEntity", "game", "GameEntity");
    test_somewhat_entity_id_example("UNKNOWN HUMAN PLAYER", "unknown", "UNKNOWN HUMAN PLAYER");
    test_somewhat_entity_id_example("The Innkeeper", "innkeeper", "The Innkeeper");
    test_somewhat_entity_id_example("RandomPlayer#1234", "player", "RandomPlayer#1234");
}

defre!(
    TAG_CHANGE_ID,
    test_TAG_CHANGE_id,
    &(r"^TAG_CHANGE Entity=(".to_string()
        + &SOMEWHAT_ENTITY_ID_INCL
        + r") tag=(\S*) value=(\S*)(?: (DEF CHANGE))? ?$") =>
    r"TAG_CHANGE Entity=GameEntity tag=TURN value=1 ", // space is intentional
    "GameEntity", "TURN", "1", None;
    r"TAG_CHANGE Entity=2 tag=WHIZBANG_DECK_ID value=0 DEF CHANGE",
    "2", "WHIZBANG_DECK_ID", "0", "DEF CHANGE";
);

// TODO gather all data for *_DESC
defre!(
    TAG_CHANGE_DESC,
    test_TAG_CHANGE_desc,
    r"^TAG_CHANGE Entity=\[.*\bid=(\d+)\b.*] tag=(\S*) value=(\S*)(?: (DEF CHANGE))? ?$" =>
    "TAG_CHANGE Entity=[entityName=Explodinator id=7 zone=HAND zonePos=5 cardId=BOT_532 player=1] tag=ZONE_POSITION value=4",
    "7", "ZONE_POSITION", "4", None;
    "TAG_CHANGE Entity=[entityName=Drustvar Horror id=141 zone=SETASIDE zonePos=0 cardId=DAL_431t player=2] tag=1304 value=142 DEF CHANGE",
    "141", "1304", "142", "DEF CHANGE";
);

defre!(
    SHOW_ENTITY_ID,
    test_SHOW_ENTITY_id,
    r"^SHOW_ENTITY - Updating Entity=(\d+) CardID=([A-Z_0-9a-z]*)$" =>
    "SHOW_ENTITY - Updating Entity=87 CardID=DAL_710e",
    "87",
    "DAL_710e";
);

defre!(
    SHOW_ENTITY_DESC,
    test_SHOW_ENTITY_desc,
    r"^SHOW_ENTITY - Updating Entity=\[.*\bid=(\d+)\b.*] CardID=([A-Z_0-9a-z]*)$" =>
    "SHOW_ENTITY - Updating Entity=[entityName=UNKNOWN ENTITY [cardType=INVALID] id=48 \
     zone=HAND zonePos=3 cardId= player=2] CardID=GIL_580",
    "48",
    "GIL_580";
);

defre!(
    HIDE_ENTITY_ID,
    test_HIDE_ENTITY_id,
    r"^HIDE_ENTITY - Entity=(\d+) tag=(\S*) value=(\S*)$" =>
    "HIDE_ENTITY - Entity=90 tag=ZONE value=DECK",
    "90",
    "ZONE",
    "DECK";
);

defre!(
    HIDE_ENTITY_DESC,
    test_HIDE_ENTITY_desc,
    r"^HIDE_ENTITY - Entity=\[.*\bid=(\d+)\b.*] tag=(\S*) value=(\S*)$" =>
    "HIDE_ENTITY - Entity=[entityName=Soul of the Murloc id=88 zone=PLAY zonePos=0 \
     cardId=DAL_710e player=1] tag=ZONE value=PLAY",
    "88",
    "ZONE",
    "PLAY";
);

defre!(
    CHANGE_ENTITY_ID,
    test_CHANGE_ENTITY_id,
    r"^CHANGE_ENTITY - Updating Entity=(\d+) CardID=([A-Z_0-9a-z]*)$" =>
    "CHANGE_ENTITY - Updating Entity=144 CardID=TRL_343",
    "144",
    "TRL_343";
);

defre!(
    CHANGE_ENTITY_DESC,
    test_CHANGE_ENTITY_desc,
    r"^CHANGE_ENTITY - Updating Entity=\[.*\bid=(\d+)\b.*] CardID=([A-Z_0-9a-z]*)$" =>
    "CHANGE_ENTITY - Updating Entity=[entityName=Sludge Slurper id=195 zone=PLAY zonePos=3 \
     cardId=DAL_433 player=2] CardID=BOT_414",
    "195",
    "BOT_414";
);

defre!(
    BLOCK_START_PLACEHOLDER,
    test_BLOCK_START_placeholder,
    r"^BLOCK_START.*$" =>
    "BLOCK_START BlockType=TRIGGER Entity=troonie#2769 EffectCardId= EffectIndex=-1 Target=0 SubOption=-1 TriggerKeyword=0";
);

defre!(BLOCK_END, test_BLOCK_END, r"^BLOCK_END$" => "BLOCK_END",);

defre!(
    META_DATA_PLACEHOLDER,
    test_META_DATA_placeholder,
    r"^META_DATA - Meta=(DAMAGE|TARGET|EFFECT_TIMING|HISTORY_TARGET|OVERRIDE_HISTORY|SHOW_BIG_CARD|HEALING|EFFECT_SELECTION|HISTORY_TARGET_DONT_DUPLICATE_UNTIL_END|BURNED_CARD|ARTIFICIAL_PAUSE|BEGIN_ARTIFICIAL_HISTORY_TILE|CONTROLLER_AND_ZONE_CHANGE|HOLD_DRAWN_CARD) Data=(\d+) Info(?:Count)?=(\d+)$" =>
// Since update at 2019-08-01, `Info` has changed to `InfoCount`
    "META_DATA - Meta=CONTROLLER_AND_ZONE_CHANGE Data=0 Info=5",
    "CONTROLLER_AND_ZONE_CHANGE",
    "0",
    "5";
    "META_DATA - Meta=TARGET Data=0 InfoCount=1",
    "TARGET",
    "0",
    "1";
);

defre!(
    INFO_PLACEHOLDER,
    test_INFO_placeholder,
    r"^Info\[(\d+)] = (.*)$" =>
    "Info[2] = GameEntity",
    "2",
    "GameEntity";
);

defre!(
    SUB_SPELL_START, test_SUB_SPELL_START,
    r"^SUB_SPELL_START - SpellPrefabGUID=(\S+) Source=(\d+) TargetCount=(\d+)$" =>
    r"SUB_SPELL_START - SpellPrefabGUID=PlagueofFlames_OverrideDeath:af418501bb3ac834a828233ab5905801 Source=177 TargetCount=0",
    "PlagueofFlames_OverrideDeath:af418501bb3ac834a828233ab5905801", "177", "0";
);

defre!(SUB_SPELL_END, test_SUB_SPELL_END,
    r"^SUB_SPELL_END$" =>
    "SUB_SPELL_END";
);

defre!(GameState_DebugPrintGame_LOG, test_GameState_DebugPrintGame_log,
r"^D \d\d:\d\d:\d\d(?:\.\d{7})? GameState\.DebugPrintGame\(\) - (.*)$"
=>
r"D 20:10:21.3437410 GameState.DebugPrintGame() - PlayerID=2, PlayerName=UNKNOWN HUMAN PLAYER",
"PlayerID=2, PlayerName=UNKNOWN HUMAN PLAYER";
);

defre!(GameState_DebugPrintGame_PLAYER_DATA, test_GameState_DebugPrintGame_player_data,
&(r"^PlayerID=([12]), PlayerName=(".to_string() + SOMEWHAT_ENTITY_ID_INCL + ")$")
=>
r"PlayerID=2, PlayerName=UNKNOWN HUMAN PLAYER",
"2",
"UNKNOWN HUMAN PLAYER";
);
