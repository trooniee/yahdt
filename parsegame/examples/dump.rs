use parsegame::{Parser};
use std::io::{self, BufRead};

fn main() -> Result<(), io::Error> {
    simple_logger::init().unwrap();
    let mut parser = Parser::new();
    let mut line = String::new();
    let stdin = io::stdin();
    let mut stdin = stdin.lock();
    loop {
        line.clear();
        stdin.read_line(&mut line)?;
        if line.is_empty() { break }
        parser.feed_line(&line);
        while parser.has_output() {
            println!("{:?}", parser.pop().unwrap());
        }
    };
    parser.feed_end();
    while parser.has_output() {
        println!("{:?}", parser.pop().unwrap());
    }

    Ok(())
}
