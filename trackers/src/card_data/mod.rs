//! Access to cards database: card data and images
use parsegame::types::{CardDbId, CardId};
use serde_json::Value;
use std::collections::HashMap;
use std::convert::TryInto;

pub struct Card {
    pub dbf_id: CardDbId,
    pub card_id: CardId,
    pub name: String,
    pub mana_cost: Option<i32>,
}

lazy_static::lazy_static! {
    static ref CARDS: Vec<Card> = {
        static RAW_JSON: &'static str = include_str!("cards.json");
        let json: Value = serde_json::from_str(RAW_JSON).unwrap();
        let mut vec: Vec<Card> = Vec::new();
        for card in json.as_array().unwrap() {
            let _ = (|| -> Result<(), ()> {
                vec.push(Card {
                    // TODO replace unwraps here with error logging
                    card_id: card.get("id").ok_or(())?.as_str().unwrap().to_owned(),
                    name: card.get("name").ok_or(())?.as_str().unwrap().to_owned(),
                    dbf_id: card.get("dbfId").ok_or(())?.as_i64().unwrap().try_into().unwrap(),
                    mana_cost: card.get("cost").map(|value| value.as_i64().unwrap().try_into().unwrap()),
                });
                Ok(())
            })();
        }
        vec
    };
    pub static ref CARDS_ID: HashMap<&'static CardId, &'static Card> = {
        CARDS.iter().map(|card| (&card.card_id, card)).collect()
    };
    pub static ref CARDS_DB: HashMap<&'static CardDbId, &'static Card> = {
        CARDS.iter().map(|card| (&card.dbf_id, card)).collect()
    };
}
