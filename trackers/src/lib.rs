mod card_data;
mod deck_list;
pub use card_data::{CARDS_DB, CARDS_ID};
pub use deck_list::{DeckList, DeckListGet};

mod card_info;
mod game;
mod native;
mod player_side;
mod tags;
use {card_info::*, game::*, native::*, player_side::*, tags::*};
pub use card_info::CardInfo;

use parsegame::GameEvent;
use parsegame::tags::Zone;

pub struct DeckTracker {
    pub game: GameTracker,
    pub tags: TagsTracker,
    pub natives: NativeTracker,
    pub sides: PlayerSideTracker,
    pub card_info: CardInfoTracker,
    pub old_tags: TagsTracker,
}

impl DeckTracker {
    pub fn new() -> Self {
        DeckTracker {
            game: Default::default(),
            tags: Default::default(),
            natives: Default::default(),
            sides: Default::default(),
            card_info: Default::default(),
            old_tags: Default::default(),
        }
    }

    pub fn send(&mut self, event: &GameEvent) {
        self.game.send(event);
        self.tags.send(event);
        self.natives.send(event, NativeTrackerContext {
            card_info: &self.card_info,
            tags: &self.tags,
            game: &self.game,
            old_tags: &self.old_tags,
        });
        self.sides.send(event, PlayerSideTrackerContext {
            card_info: &self.card_info,
            game: &self.game,
            tags: &self.tags,
        });
        self.card_info.send(event);
        self.old_tags.send(event);
    }

    pub fn sides_defined(&self) -> bool {
        self.sides.sides().is_some()
    }

    pub fn our_natives_outside_deck(&self) -> DeckList {
        let controller = self.sides.sides().unwrap().0;
        let mut deck_list = DeckList::new();
        for entity_id in self.natives.iter_natives() {
            let tags = self.tags.get(&entity_id);
            if tags.controller == Some(controller) && tags.zone != Some(Zone::Deck) {
                deck_list.insert(self.card_info.get(entity_id).clone());
            }
        }
        deck_list.extend(self.natives.iter_lost(controller).cloned());
        deck_list
    }

    pub fn our_natives_left_in_deck(&self, deck: &DeckList) -> DeckList {
        deck - self.our_natives_outside_deck() // TODO reverse subtraction for 1 less alloc
    }

    pub fn our_foreigns_in_deck(&self) -> DeckList {
        let controller = self.sides.sides().unwrap().0;
        let mut deck_list = DeckList::new();
        for entity_id in self.natives.iter_foreigns() {
            let tags = self.tags.get(&entity_id);
            if tags.controller == Some(controller) && tags.zone == Some(Zone::Deck) {
                deck_list.insert(self.card_info.get(entity_id).clone());
            }
        }
        deck_list
    }

    pub fn their_natives_out(&self) -> DeckList {
        let controller = self.sides.sides().unwrap().1;
        let mut deck_list = DeckList::new();
        for entity_id in self.natives.iter_natives() {
            let tags = self.tags.get(&entity_id);
            if tags.controller == Some(controller)
                && tags.zone != Some(Zone::Deck)
                && tags.zone != Some(Zone::Hand)
            {
                deck_list.insert(self.card_info.get(entity_id).clone());
            }
        }
        deck_list.extend(self.natives.iter_lost(controller).cloned());
        deck_list
    }

    pub fn their_foreigns_in_deck(&self) -> DeckList {
        let controller = self.sides.sides().unwrap().1;
        let mut deck_list = DeckList::new();
        for entity_id in self.natives.iter_foreigns() {
            let tags = self.tags.get(&entity_id);
            if tags.controller == Some(controller) && tags.zone == Some(Zone::Deck) {
                deck_list.insert(self.card_info.get(entity_id).clone());
            }
        }
        deck_list
    }

    pub fn their_foreigns_in_hand(&self) -> DeckList {
        let controller = self.sides.sides().unwrap().1;
        let mut deck_list = DeckList::new();
        for entity_id in self.natives.iter_foreigns() {
            let tags = self.tags.get(&entity_id);
            if tags.controller == Some(controller) && tags.zone == Some(Zone::Hand) {
                deck_list.insert(self.card_info.get(entity_id).clone());
            }
        }
        deck_list
    }
}
