use crate::CardInfo;
use std::collections::HashMap;
use std::ops;

#[derive(Default, Clone)]
pub struct DeckList {
    // TODO make non-public
    pub map: HashMap<CardInfo, i64>,
}

impl DeckList {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn is_empty(&self) -> bool {
        self.map.values().all(|v| v == &0)
    }

    pub fn insert(&mut self, card: CardInfo) {
        self.insert_amount(card, 1);
    }

    pub fn insert_amount(&mut self, card: CardInfo, amount: i64) {
        *self.map.entry(card).or_insert(0) += amount;
    }

    pub fn extend(&mut self, from: impl Iterator<Item = CardInfo>) {
        for card in from {
            self.insert(card);
        }
    }
}

pub trait DeckListGet<T> {
    fn get(&self, key: T) -> i64;
}

impl<T> DeckListGet<T> for DeckList
where T: Into<String> {
    fn get(&self, key: T) -> i64 {
        *self.map.get(&CardInfo::Revealed(Into::<String>::into(key))).unwrap_or(&0)
    }
}

// TODO test ops::* (for result and for infinite recursion)
// TODO `Add*` with macro
impl ops::SubAssign<DeckList> for DeckList {
    fn sub_assign(&mut self, rhs: DeckList) {
        for (card, amount) in rhs.map {
            match self.map.get_mut(&card) {
                Some(val) => *val -= amount,
                None => {
                    self.map.insert(card, -amount);
                }
            };
        }
    }
}

impl ops::SubAssign<&DeckList> for DeckList {
    fn sub_assign(&mut self, rhs: &DeckList) {
        for (card, amount) in rhs.map.iter() {
            match self.map.get_mut(&card) {
                Some(val) => *val -= amount,
                None => {
                    self.map.insert(card.clone(), -*amount);
                }
            };
        }
    }
}

// TODO combine the following two with trait SubAssign<T> for argument
impl ops::Sub<&DeckList> for DeckList {
    type Output = DeckList;
    fn sub(mut self, rhs: &DeckList) -> Self::Output {
        self -= rhs;
        self
    }
}

impl ops::Sub<DeckList> for DeckList {
    type Output = DeckList;
    fn sub(mut self, rhs: DeckList) -> Self::Output {
        self -= rhs;
        self
    }
}

// TODO combine the following two with trait Sub<T> for argument
impl ops::Sub<&DeckList> for &DeckList {
    type Output = DeckList;
    fn sub(self, rhs: &DeckList) -> Self::Output {
        self.clone() - rhs
    }
}

impl ops::Sub<DeckList> for &DeckList {
    type Output = DeckList;
    fn sub(self, rhs: DeckList) -> Self::Output {
        self.clone() - rhs
    }
}
