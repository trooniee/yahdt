use crate::CARDS_DB;
use lazy_static::lazy_static;
use parsegame::tags::Zone;
use parsegame::{CardDbId, EntityId, GameEvent, PlayerId};

#[derive(Default, Clone)]
pub struct Tags {
    pub controller: Option<PlayerId>,
    pub creator_dbid: Option<CardDbId>,
    pub zone: Option<Zone>,
}

pub struct TagsTracker {
    vec: Vec<Tags>,
}

impl Default for TagsTracker {
    fn default() -> Self {
        TagsTracker { vec: Vec::new() }
    }
}

impl TagsTracker {
    pub fn send(&mut self, event: &GameEvent) {
        match event {
            GameEvent::CreateGame => *self = Default::default(),
            GameEvent::EntityUpdate {
                entity_id, tags, ..
            } => {
                let rec: &mut _ = self.get_mut(entity_id);
                // TODO use macros. name them similar to macros in parsegame::tags.
                if let Some(creator) = tags.CREATOR_DBID() {
                    rec.creator_dbid = Some(creator);
                    let db_rec = CARDS_DB.get(&creator);
                    match db_rec {
                        None => log::trace!("#{}: CREATOR_DBID := #{}", entity_id, creator),
                        Some(ref db_rec) => log::trace!(
                            "#{}: CREATOR_DBID := #{} [{}]",
                            entity_id,
                            creator,
                            db_rec.name
                        ),
                    }
                }
                if let Some(controller) = tags.CONTROLLER() {
                    if Some(controller) != rec.controller {
                        rec.controller = Some(controller);
                        log::trace!("#{}: CONTROLLER := {:?}", entity_id, controller);
                    }
                }
                if let Some(zone) = tags.ZONE() {
                    if Some(zone) != rec.zone {
                        rec.zone = Some(zone);
                        log::trace!("#{}: ZONE := {:?}", entity_id, zone);
                    }
                }
            }
            _ => {}
        }
    }

    pub fn get(&self, entity_id: &EntityId) -> &Tags {
        lazy_static! {
            static ref DEFAULT: Tags = Tags::default();
        }
        self.vec.get(*entity_id).unwrap_or(&DEFAULT)
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item=(EntityId, &Tags)> + 'a {
        self.vec.iter().enumerate()
    }

    fn get_mut(&mut self, entity_id: &EntityId) -> &mut Tags {
        if self.vec.len() <= *entity_id {
            self.vec.resize(*entity_id + 1, Default::default());
        }
        self.vec.get_mut(*entity_id).unwrap()
    }
}
