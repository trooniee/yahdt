use crate::card_info::{CardInfo, CardInfoTracker};
use crate::game::GameTracker;
use crate::tags::TagsTracker;
use parsegame::{GameEvent, PlayerId};
use parsegame::tags::{GameStep, Zone};

/// Tracks which player entity represents our side
#[derive(PartialEq)]
pub enum PlayerSideTracker {
    GameStarted,
    Sides(PlayerId, PlayerId),
    Invalid,
}

pub struct PlayerSideTrackerContext<'a> {
    pub card_info: &'a CardInfoTracker,
    pub game: &'a GameTracker,
    pub tags: &'a TagsTracker,
}

impl Default for PlayerSideTracker {
    fn default() -> Self {
        PlayerSideTracker::Invalid
    }
}

impl PlayerSideTracker {
    pub fn sides(&self) -> Option<(PlayerId, PlayerId)> {
        match self {
            &PlayerSideTracker::Sides(ref our, ref other) => Some((*our, *other)),
            _ => None,
        }
    }

    pub fn send(&mut self, event: &GameEvent, ctx: PlayerSideTrackerContext) {
        match event {
            GameEvent::CreateGame => *self = PlayerSideTracker::GameStarted,
            GameEvent::EntityUpdate {
                entity_id,
                tags, ..
            } if *self == PlayerSideTracker::GameStarted
            && Some(*entity_id) == ctx.game.entity_id
            && tags.NEXT_STEP() == Some(GameStep::BeginMulligan) => {
                // calculate revealed cards in both hands
                *self = match self.open_hand(&ctx) {
                    Some(i) => PlayerSideTracker::Sides(i, 3-i),
                    None => {
                        log::error!("Couldn't identify sides");
                        PlayerSideTracker::Invalid
                    }
                };
            }
            _ => {}
        }
    }

    fn open_hand(&self, ctx: &PlayerSideTrackerContext) -> Option<PlayerId> {
        let mut open_cards = vec![0, 0, 0];
        for (entity_id, tag) in ctx.tags.iter() {
            if let Some(controller) = tag.controller {
                if tag.zone == Some(Zone::Hand) {
                    if let CardInfo::Revealed(_) = ctx.card_info.get(&entity_id) {
                        if open_cards.len() <= controller {
                            open_cards.resize(controller + 1, 0);
                        }
                        *open_cards.get_mut(controller)? += 1;
                    }
                }
            }
        }
        Some(
            if open_cards.get(1)? > open_cards.get(2)? { 1 } else { 2 }
        )
    }
}
