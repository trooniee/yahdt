use crate::CARDS_ID;
use lazy_static::lazy_static;
use parsegame::{CardId, EntityId, GameEvent};

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
pub enum CardInfo {
    Unknown,
    Revealed(CardId),
}

impl Default for CardInfo {
    fn default() -> Self {
        CardInfo::Unknown
    }
}

/// Tracks all available card id information
pub struct CardInfoTracker {
    vec: Vec<CardInfo>,
}

impl Default for CardInfoTracker {
    fn default() -> Self {
        Self {
            vec: Vec::with_capacity(69),
        }
    }
}

impl CardInfoTracker {
    pub fn send(&mut self, event: &GameEvent) {
        match event {
            GameEvent::CreateGame => *self = Default::default(),
            GameEvent::EntityUpdate {
                entity_id, card_id, ..
            } => {
                let rec: &mut _ = self.get_mut(entity_id);
                if let Some(Some(new_card_id)) = card_id {
                    *rec = CardInfo::Revealed(new_card_id.into());
                    let db_rec = CARDS_ID.get(new_card_id);
                    match db_rec {
                        None => log::trace!("#{}: card_id := {:?}", entity_id, new_card_id),
                        Some(ref db_rec) => log::trace!(
                            "#{}: card_id := {:?} [{}]",
                            entity_id,
                            new_card_id,
                            db_rec.name
                        ),
                    }
                }
            }
            _ => {}
        }
    }

    pub fn get(&self, entity_id: &EntityId) -> &CardInfo {
        lazy_static! {
            static ref DEFAULT: CardInfo = CardInfo::default();
        }
        self.vec.get(*entity_id).unwrap_or(&DEFAULT)
    }

    fn get_mut(&mut self, entity_id: &EntityId) -> &mut CardInfo {
        if self.vec.len() <= *entity_id {
            self.vec.resize(*entity_id + 1, Default::default());
        }
        self.vec.get_mut(*entity_id).unwrap()
    }
}
