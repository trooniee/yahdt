use parsegame::{EntityId, EntityUpdateMetadata, GameEvent};
use parsegame::tags::{GameState, GameStep};

#[derive(Default)]
pub struct GameTracker {
    pub entity_id: Option<EntityId>,
    pub next_step: Option<GameStep>,
    pub step: Option<GameStep>,
    pub state: Option<GameState>,
}

impl GameTracker {
    pub fn send(&mut self, event: &GameEvent) {
        match event {
            GameEvent::CreateGame {..} => *self = Default::default(),
            GameEvent::EntityUpdate {
                entity_id,
                tags,
                metadata,
                ..
            } => {
                if let EntityUpdateMetadata::GameEntity{..} = metadata  {
                    *self = Default::default();
                    self.entity_id = Some(*entity_id);
                }
                if Some(*entity_id) == self.entity_id {
                    if let Some(next_step) = tags.NEXT_STEP() {
                        self.next_step = Some(next_step);
                        log::trace!("#{}: NEXT_STEP := {:?}", entity_id, next_step);
                    }
                    if let Some(step) = tags.STEP() {
                        self.next_step = Some(step);
                        log::trace!("#{}: STEP := {:?}", entity_id, step);
                    }
                    if let Some(state) = tags.STATE() {
                        self.state = Some(state);
                        log::trace!("#{}: STATE := {:?}", entity_id, state);
                    }
                }
            }
            _ => (),
        }
    }
}
