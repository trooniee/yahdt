use crate::card_info::{CardInfo, CardInfoTracker};
use crate::game::GameTracker;
use crate::tags::TagsTracker;
use parsegame::{EntityId, EntityUpdateMetadata, GameEvent, PlayerId};
use parsegame::tags::Zone;
use std::collections::{HashMap, HashSet};

/// Track native cards
///
/// I.e. cards initially in the deck
#[derive(Default)]
pub struct NativeTracker {
    set: HashSet<EntityId>,
    foreign: HashSet<EntityId>,
    lost: HashMap<PlayerId, Vec<CardInfo>>,
}

pub struct NativeTrackerContext<'a> {
    pub card_info: &'a CardInfoTracker,
    pub game: &'a GameTracker,
    pub tags: &'a TagsTracker,
    pub old_tags: &'a TagsTracker,
}

impl NativeTracker {
    pub fn send(&mut self, event: &GameEvent, ctx: NativeTrackerContext) {
        match event {
            GameEvent::CreateGame => *self = Default::default(),
            GameEvent::EntityUpdate {
                entity_id,
                metadata: EntityUpdateMetadata::ChangeEntityUpdating { .. },
                ..
            } => {
                if self.set.remove(entity_id) {
                    log::trace!("Card #{} is transformed and not native any more", entity_id);
                    if let Some(controller) = ctx.tags.get(entity_id).controller {
                        let lost_vec: &mut _ = self.lost.entry(controller).or_insert_with(Default::default);
                        lost_vec.push(ctx.card_info.get(entity_id).clone());
                    }
                    self.foreign.insert(*entity_id);
                }
            }
            GameEvent::EntityUpdate {
                entity_id,
                metadata: EntityUpdateMetadata::FullEntityCreating { .. },
                ..
            } => if ctx.game.state == None && ctx.tags.get(entity_id).zone == Some(Zone::Deck) {
                self.set.insert(*entity_id);
            } else {
                self.foreign.insert(*entity_id);
            }
            GameEvent::EntityUpdate {
                entity_id,
                tags,
                ..
            } if tags.CONTROLLER().is_some() => {
                if let Some(old_controller) = ctx.old_tags.get(entity_id).controller {
                    let controller = tags.CONTROLLER().unwrap();
                    if old_controller != controller {
                        if self.set.remove(entity_id) {
                            log::trace!("Card #{} changes controller and is not native any more", entity_id);
                            let lost_vec: &mut _ = self.lost.entry(old_controller).or_insert_with(Default::default);
                            lost_vec.push(ctx.card_info.get(entity_id).clone());
                            self.foreign.insert(*entity_id);
                        }
                    }
                }
            }
            _ => {}
        }
    }

    pub fn iter_natives(&self) -> impl Iterator<Item = &EntityId> {
        self.set.iter()
    }

    pub fn iter_foreigns(&self) -> impl Iterator<Item = &EntityId> {
        self.foreign.iter()
    }

    pub fn iter_lost(&self, controller:PlayerId) -> impl Iterator<Item = &CardInfo> {
        static EMPTY: [CardInfo; 0] = [];
        self.lost.get(&controller).map_or_else(|| EMPTY.iter(), |vec| vec.iter())
    }

    pub fn is_native(&self, entity_id: EntityId) -> bool {
        self.set.contains(&entity_id)
    }
}
