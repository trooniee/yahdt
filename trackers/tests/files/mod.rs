use libflate::gzip::Decoder;
use parsegame::Parser;
use std::io::{BufRead, BufReader, Read};
use trackers::DeckTracker;

pub static REFERENCE: &'static[u8] = include_bytes!("game-2019-06-16-20-38-26-503610.log.gz");
pub static DRUID_WARLOCK_SIDES_MESSED: &'static[u8] = include_bytes!("game-2019-08-01-04-02-48-362067.log.gz");
pub static CABAL_SHADOWPRIEST: &'static[u8] = include_bytes!("game-2019-08-15-20-13-43-936246.log.gz");

pub struct Test<R: Read> {
    reader: BufReader<Decoder<R>>,
    line_number: u32,
    pub parser: Parser,
    pub tracker: DeckTracker,
}

impl<R: Read> Test<R> {
    pub fn new(source: R) -> Self {
        Self {
            reader: BufReader::new(Decoder::new(source).unwrap()),
            parser: Parser::new(),
            tracker: DeckTracker::new(),
            line_number: 0,
        }
    }
    fn line(&mut self) -> bool {
        let mut s = String::new();
        self.reader.read_line(&mut s).unwrap();
        if !s.ends_with('\n') {
            return false;
        }
        self.line_number += 1;
        self.parser.feed_line(&s);
        while let Some(event) = self.parser.pop() {
            self.tracker.send(&event);
        }
        true
    }
    pub fn up_to_line(&mut self, lines: u32) {
        while self.line_number < lines {
            if !self.line() {
                panic!("Unexpected EOF");
            }
        }
        self.parser.feed_pause();
        while let Some(event) = self.parser.pop() {
            self.tracker.send(&event);
        }
    }
    pub fn finish(&mut self) {
        while self.line() {}
        self.parser.feed_end();
        while let Some(event) = self.parser.pop() {
            self.tracker.send(&event);
        }
    }
}
