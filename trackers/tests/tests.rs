mod files;
use files::*;
use trackers::DeckListGet;

lazy_static::lazy_static! {
    static ref LOGGER: () = simple_logger::init().unwrap();
}

#[test]
fn druid_warlock_sides_messed() {
    *LOGGER;
    let mut test = Test::new(REFERENCE);
    test.up_to_line(1201);
    assert_eq!(test.tracker.sides.sides(), Some((1, 2)));

    let mut test = Test::new(DRUID_WARLOCK_SIDES_MESSED);
    // The Coin has id=76 for some reason (the opponent is on coin, so it's hidden).
    test.up_to_line(1356);
    assert_eq!(test.tracker.sides.sides(), Some((2, 1)));
}

#[test]
fn natives_start() {
    *LOGGER;
    let mut test = Test::new(REFERENCE);
    test.up_to_line(999);
    for i in 1..=3 {
        if test.tracker.natives.is_native(i) {
            panic!("Entity #{} should NOT be native", i);
        }
    }
    for i in 4..=63 {
        if !test.tracker.natives.is_native(i) {
            panic!("Entity #{} should BE native", i);
        }
    }
    for i in 64..=67 {
        if test.tracker.natives.is_native(i) {
            panic!("Entity #{} should NOT be native", i);
        }
    }
}

#[test]
fn natives_steal() {
    *LOGGER;
    let mut test = Test::new(CABAL_SHADOWPRIEST);
    test.up_to_line(14522);
    assert_eq!(test.tracker.our_natives_outside_deck().get("BOT_537"), 0);
}
