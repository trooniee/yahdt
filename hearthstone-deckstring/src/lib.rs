mod vlq;
use vlq::*;
#[cfg(test)] mod test;

use std::collections::{hash_map::Entry, HashMap};
use std::convert::TryFrom;

pub type DbfId = u64;

#[derive(Debug, PartialEq, Eq)]
pub enum Format {
    Unknown,
    Wild,
    Standard,
    Other(u64),
}

#[derive(Debug, PartialEq, Eq)]
pub struct Deck {
    pub format: Format,
    pub hero: DbfId,
    pub cards: HashMap<DbfId, u64>,
}

// TODO differentiate errors
#[derive(Debug, PartialEq)]
pub struct Error;

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error parsing deckstring")
    }
}

impl std::error::Error for Error {}

impl TryFrom<&str> for Deck {
    type Error = Error;
    fn try_from(s: &str) -> Result<Self, Self::Error> {
        Self::from_long(s)
    }
}

impl std::fmt::Display for Deck {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.into_short())
    }
}

impl Deck {
    pub fn into_short(&self) -> String {
        let mut buf = Vec::new();
        write_vlq(&mut buf, 0u8); // header
        write_vlq(&mut buf, 1u8); // version
        write_vlq(&mut buf, match self.format {
            Format::Unknown => 0,
            Format::Wild => 1,
            Format::Standard => 2,
            Format::Other(x) => x,
        });
        // heros:
        write_vlq(&mut buf, 1u8);
        write_vlq(&mut buf, self.hero);
        // one-ofs:
        let mut vec: Vec<DbfId> = self.cards.iter()
            .filter_map(|(id, amount)| if *amount == 1 { Some(*id) } else { None })
            .collect();
        vec.sort_unstable();
        write_vlq(&mut buf, vec.len());
        for id in vec {
            write_vlq(&mut buf, id);
        }
        // two-ofs:
        let mut vec: Vec<DbfId> = self.cards.iter()
            .filter_map(|(id, amount)| if *amount == 2 { Some(*id) } else { None })
            .collect();
        vec.sort_unstable();
        write_vlq(&mut buf, vec.len());
        for id in vec {
            write_vlq(&mut buf, id);
        }
        // multiples:
        let mut vec: Vec<(DbfId, u64)> = self.cards.iter()
            .filter_map(|(id, amount)| if *amount > 2 { Some((*id, *amount)) } else { None })
            .collect();
        vec.sort_unstable();
        write_vlq(&mut buf, vec.len());
        for (id, amount) in vec {
            write_vlq(&mut buf, id);
            write_vlq(&mut buf, amount);
        }
        base64::encode(&buf)
    }

    pub fn from_long(s: &str) -> Result<Self, Error> {
        let mut lines = s
            .split('\n')
            .map(str::trim)
            .filter(|s| !s.is_empty() && !s.starts_with('#'));
        let line: &str = lines.next().ok_or(Error)?;
        if lines.next().is_some() {
            return Err(Error); // more than one string
        }
        Self::from_short(line)
    }

    pub fn from_short(s: &str) -> Result<Self, Error> {
        let bytes = base64::decode(s).or(Err(Error))?;
        let bytes = &mut bytes.iter().cloned();
        if read_vlq(bytes) != Ok(0) {
            // header
            return Err(Error);
        }
        if read_vlq(bytes) != Ok(1) {
            // version
            return Err(Error);
        }
        let format = match read_vlq(bytes) {
            Ok(0) => Format::Unknown,
            Ok(1) => Format::Wild,
            Ok(2) => Format::Standard,
            Ok(x) => Format::Other(x),
            Err(_) => return Err(Error),
        };
        if read_vlq(bytes) != Ok(1) {
            // number of heroes
            return Err(Error);
        }
        let hero: DbfId = read_vlq(bytes).or(Err(Error))?;
        let mut map = HashMap::new();

        let n = read_vlq(bytes).or(Err(Error))?;
        for _ in 0..n {
            let e = map.entry(read_vlq(bytes).or(Err(Error))?);
            match e {
                Entry::Occupied(_) => return Err(Error),
                Entry::Vacant(v) => v.insert(1),
            };
        }
        let n = read_vlq(bytes).or(Err(Error))?;
        for _ in 0..n {
            let e = map.entry(read_vlq(bytes).or(Err(Error))?);
            match e {
                Entry::Occupied(_) => return Err(Error),
                Entry::Vacant(v) => v.insert(2),
            };
        }
        let n = read_vlq(bytes).or(Err(Error))?;
        for _ in 0..n {
            let e = map.entry(read_vlq(bytes).or(Err(Error))?);
            let amount = read_vlq(bytes).or(Err(Error))?;
            match e {
                Entry::Occupied(_) => return Err(Error),
                Entry::Vacant(v) => v.insert(amount),
            };
        }

        Ok(Deck {
            format,
            hero,
            cards: map,
        })
    }
}
