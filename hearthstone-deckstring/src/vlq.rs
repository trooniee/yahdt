#[derive(Debug, PartialEq, Eq)]
pub enum VlqError {
    Eof,
    UnexpectedEof,
    TooBig,
}

pub fn read_vlq(it: &mut impl Iterator<Item = u8>) -> Result<u64, VlqError> {
    let start = it.next().ok_or(VlqError::Eof)?;
    let mut has_next = start & 0x80;
    let mut value: u64 = (start & 0x7F).into();
    let mut shift: u64 = 1;

    while has_next != 0 {
        let byte = it.next().ok_or(VlqError::UnexpectedEof)?;
        has_next = byte & 0x80;
        let sept: u64 = (byte & 0x7F).into();
        shift = shift.checked_mul(0x80).ok_or(VlqError::TooBig)?;
        let sept = sept.checked_mul(shift).ok_or(VlqError::TooBig)?;
        value = value.checked_add(sept.into()).ok_or(VlqError::TooBig)?;
    }

    Ok(value)
}

pub trait WriteVlq: Copy {
    fn as_u8(self) -> u8;
    fn is_zero(self) -> bool;
    fn shr_7(self) -> Self;
}

macro_rules! write_vlq {
    ( $($t:ty),* $(,)? ) => {
        $(
            impl WriteVlq for $t {
                fn as_u8(self) -> u8 { self as u8 }
                fn is_zero(self) -> bool { self == 0 }
                fn shr_7(self) -> Self { self >> 7 }
            }
        )*
    }
}
write_vlq!(usize, u8, u16, u32, u64, u128);

pub fn write_vlq<T: WriteVlq>(buf: &mut Vec<u8>, mut value: T) {
    loop {
        buf.push(value.as_u8() | 0x80);
        value = value.shr_7();
        if value.is_zero() {
            break;
        }
    }
    *buf.last_mut().unwrap() &= 0x7f;
}

#[cfg(test)]
mod tests {
    use super::*;

    fn success1(input: &[u8], expect: u64) {
        let mut iter = input.iter().cloned();
        assert_eq!(read_vlq(&mut iter), Ok(expect));
        if read_vlq(&mut iter) != Err(VlqError::Eof) {
            panic!("EOF expected");
        }

        let mut buf = Vec::new();
        write_vlq(&mut buf, expect);
        assert_eq!(buf, input);
    }

    #[test]
    fn test1() {
        success1(&[0], 0);
        success1(&[23], 23);
        success1(&[0x85, 1], 133);
    }
}
