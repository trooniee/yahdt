use super::*;

#[test]
fn test_valid() {
    for input in [
        "AAECAQcEkvgCoIADm5QDn7cDDUuiBP8HnfACm/MC0fUCg/sCnvsCs/wC9YADl5QDmpQDkp8DAA==",
        "AAECAQcEkvgCoIADm5QDn7cDDUuiBP8HnfACm/MC0fUCg/sCnvsCs/wC9YADl5QDmpQDkp8DAA=",
        "AAECAQcEkvgCoIADm5QDn7cDDUuiBP8HnfACm/MC0fUCg/sCnvsCs/wC9YADl5QDmpQDkp8DAA",
        "### SN1P-SN4P Bomb Warrior
# Class: Warrior
# Format: Standard
# Year of the Dragon
#
# 2x (1) Eternium Rover
# 2x (1) Omega Assembly
# 2x (1) Shield Slam
# 2x (1) Town Crier
# 2x (2) Warpath
# 2x (3) Augmented Elekk
# 2x (3) Clockwork Goblin
# 2x (3) Shield Block
# 1x (3) SN1P-SN4P
# 2x (4) Militia Commander
# 2x (4) Omega Devastator
# 2x (4) Wrenchcalibur
# 2x (5) Brawl
# 2x (5) Dyn-o-matic
# 1x (5) Zilliax
# 1x (7) Blastmaster Boom
# 1x (7) Dr. Boom, Mad Genius
#
AAECAQcEkvgCoIADm5QDn7cDDUuiBP8HnfACm/MC0fUCg/sCnvsCs/wC9YADl5QDmpQDkp8DAA==
#
# To use this deck, copy it to your clipboard and create a new deck in Hearthstone
# https://www.example.com/deck-library/warrior-decks/bomb-warrior/sn1p-sn4p-bomb-warrior/",
        "AAEBAR8SjQG3ArUDhwTnB5cIxQjZCZYN9Q2NrwKtsALKvwK0kQPAmAPCmAOHmgO2nAME2wmBCs6uArr2AgHmlgME",
    ]
    .iter()
    {
        println!("Testing: {}", input);
        let deck = Deck::try_from(*input).unwrap();
        println!("{:?}", &deck);
        assert_eq!(&deck, &Deck::try_from(deck.into_short().as_str()).unwrap());
    }
}

#[test]
fn test_correct() {
    const SAMPLE: &str =
        "AAECAQcEkvgCoIADm5QDn7cDDUuiBP8HnfACm/MC0fUCg/sCnvsCs/wC9YADl5QDmpQDkp8DAA==";
    assert_eq!(
        Deck::try_from(SAMPLE),
        Ok(Deck {
            format: Format::Standard,
            hero: 7,
            cards: (&[
                (546, 2),
                (1023, 2),
                (49269, 2),
                (51739, 1),
                (47825, 2),
                (49184, 1),
                (56223, 1),
                (48691, 2),
                (51738, 2),
                (48146, 1),
                (53138, 2),
                (48515, 2),
                (48542, 2),
                (75, 2),
                (47133, 2),
                (51735, 2),
                (47515, 2),
            ])
                .iter()
                .cloned()
                .collect(),
        })
    );
}
