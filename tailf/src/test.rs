use super::*;
use std::io::{Seek, SeekFrom, Write};
use std::path::Path;
use std::thread::sleep;
use std::time::{Duration, Instant};
use tempfile::NamedTempFile;

macro_rules! flowing {
    ($t:expr) => {
        assert_eq!($t.peek_status().unwrap(), RotationStatus::Normal);
        assert_eq!($t.consume_status().unwrap(), RotationStatus::Normal);
        assert_eq!($t.consume_status().unwrap(), RotationStatus::Normal);
        assert_eq!($t.peek_status().unwrap(), RotationStatus::Normal);
    };
}
macro_rules! truncated {
    ($t:expr) => {
        assert_ne!($t.peek_status().unwrap(), RotationStatus::Normal);
        assert_ne!($t.consume_status().unwrap(), RotationStatus::Normal);
        flowing!($t);
    };
}
macro_rules! write {
    ($f:expr, $s:expr) => {
        $f.write_all($s.as_bytes()).unwrap();
        $f.flush().unwrap();
    };
}
macro_rules! read {
    ($t:expr, $s:expr) => {
        flowing!($t);
        let time = Instant::now();
        let time_limit = Duration::new(0, 100_000_000); // 100ms
        let sleep_dur = Duration::new(0, 1_000_000); // 1ms
        let exp = $s;
        let mut buf = String::new();
        $t.read_to_string(&mut buf).unwrap();
        while exp.len() > buf.len() {
            if time.elapsed() > time_limit {
                panic!("read! taking too long");
            }
            sleep(sleep_dur);
            $t.read_to_string(&mut buf).unwrap();
        }
        assert_eq!(buf, exp);
    };
}

#[test]
fn test_write_read() {
    let (mut file, temp_path) = NamedTempFile::new().unwrap().into_parts();
    let mut tailf = Tailf::watch(PathBuf::from(AsRef::<Path>::as_ref(&temp_path)));

    read!(tailf, "");
    flowing!(tailf);
    write!(file, "alpha");
    read!(tailf, "alpha");
    flowing!(tailf);
    write!(file, "beta");
    read!(tailf, "beta");
    flowing!(tailf);
}

#[test]
fn test_truncation() {
    let (mut file, temp_path) = NamedTempFile::new().unwrap().into_parts();
    let mut tailf = Tailf::watch(PathBuf::from(AsRef::<Path>::as_ref(&temp_path)));

    write!(file, "alpha");
    read!(tailf, "alpha");
    flowing!(tailf);

    file.set_len(0).unwrap();
    file.seek(SeekFrom::Start(0)).unwrap();

    truncated!(tailf);
    read!(tailf, "");
    flowing!(tailf);
    write!(file, "beta");
    read!(tailf, "beta");
    flowing!(tailf);
}

#[test]
fn test_truncation_late() {
    let (mut file, temp_path) = NamedTempFile::new().unwrap().into_parts();
    let mut tailf = Tailf::watch(PathBuf::from(AsRef::<Path>::as_ref(&temp_path)));

    write!(file, "alpha");
    read!(tailf, "alpha");
    flowing!(tailf);

    file.set_len(0).unwrap();
    file.seek(SeekFrom::Start(0)).unwrap();
    write!(file, "beta");
    // truncating and writing short data before checking `tailf`

    truncated!(tailf);
    read!(tailf, "beta");
    flowing!(tailf);
}

#[test]
fn test_unlinked() {
    let (mut file, temp_path) = NamedTempFile::new().unwrap().into_parts();
    let mut tailf = Tailf::watch(PathBuf::from(AsRef::<Path>::as_ref(&temp_path)));

    write!(file, "alpha");
    read!(tailf, "alpha");
    flowing!(tailf);

    temp_path.close().unwrap();
    truncated!(tailf);
}

#[test]
fn test_unlinked_late() {
    let (mut file, temp_path) = NamedTempFile::new().unwrap().into_parts();
    let mut tailf = Tailf::watch(PathBuf::from(AsRef::<Path>::as_ref(&temp_path)));

    write!(file, "alpha");
    read!(tailf, "alpha");
    flowing!(tailf);

    write!(file, "beta");
    temp_path.close().unwrap();

    read!(tailf, "beta");
    truncated!(tailf);
}
