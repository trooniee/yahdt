use std::convert::TryFrom;
use std::fs::{self, File};
use std::io::{self, ErrorKind, Read, Seek, SeekFrom};
//use std::os::unix::fs::MetadataExt;
use std::path::PathBuf;

#[cfg(test)]
mod test;

/// Rotation status. More variants can be added
#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum RotationStatus {
    /// The only no-truncation variant.
    Normal,
    /// Size shrinking detected
    SizeTruncated,
    /// File seems to be unlinked from the file system path
    Unlinked,
    /// File seems to be replaced with another file (rename(2))
    Replaced,
}

impl RotationStatus {
    pub fn is_normal(self) -> bool {
        self == RotationStatus::Normal
    }
    pub fn is_truncated(self) -> bool {
        !self.is_normal()
    }
}

//type UnixFileId = (u64, u64);

pub struct Tailf {
    path: PathBuf,
    file: Option<OpenedFile>,
    status: RotationStatus,
}

struct OpenedFile {
    file: File,
    seek: u64,
    //id: UnixFileId,
}

impl Tailf {
    pub fn watch(path: PathBuf) -> Self {
        Self {
            path,
            file: None,
            status: RotationStatus::Normal,
        }
    }

    fn try_open(&mut self) -> Result<(), io::Error> {
        if self.file.is_none() {
            self.file = match File::open(&self.path) {
                Ok(file) => {
                    //let metadata = file.metadata()?;
                    Some(OpenedFile {
                        file,
                        seek: 0,
                        //id: (metadata.dev(), metadata.ino()),
                    })
                }
                Err(ref error) if error.kind() == ErrorKind::NotFound => None,
                Err(error) => return Err(error),
            };
        }
        Ok(())
    }

    pub fn peek_status(&mut self) -> Result<RotationStatus, io::Error> {
        self.read(&mut [0; 0])?;
        Ok(self.status)
    }

    pub fn consume_status(&mut self) -> Result<RotationStatus, io::Error> {
        let status = self.peek_status()?;
        self.status = RotationStatus::Normal;
        Ok(status)
    }
}

impl Read for Tailf {
    fn read(&mut self, buf: &mut [u8]) -> Result<usize, io::Error> {
        // Note that buf may have zero length

        // 1. if truncation unhandled, return 0
        match self.status {
            RotationStatus::Normal => (),
            _ => return Ok(0),
        }

        // 2. if file does not exist, return 0
        self.try_open()?;
        let o: &mut OpenedFile = match self.file {
            Some(ref mut o) => o,
            None => return Ok(0),
        };

        // 3. Check for early truncations
        let metadata = match o.file.metadata() {
            Ok(m) => m,
            Err(ref error) if error.kind() == ErrorKind::NotFound => {
                panic!("This is not supposed to happen at this stage.");
                // file handle may be unlinked from the directory record, but that's
                // not supposed to cause an error. If it causes an error (on windows,
                // supposedly), this panic could be turned into Ok(0) (temporarily?), and a todo
                // should be added to rethink this behavior
            }
            Err(error) => return Err(error),
        };
        if metadata.len() < o.seek {
            self.status = RotationStatus::SizeTruncated;
            o.seek = 0;
            o.file.seek(SeekFrom::Start(0))?;
            return Ok(0);
        }

        // 4. Actual read
        let n: usize = o.file.read(buf)?;
        o.seek += u64::try_from(n).unwrap();
        if n > 0 {
            return Ok(n);
        }

        // 5.1. Check if bytes are still available in file
        // TODO ? even if they are, still perform 5.2, but set postponed truncation status. This
        // will allow to detect late truncations more reliably
        let metadata = match o.file.metadata() {
            Ok(m) => m,
            Err(ref error) if error.kind() == ErrorKind::NotFound => {
                panic!("This is not supposed to happen at this stage.");
                // file handle may be unlinked from the directory record, but that's
                // not supposed to cause an error. If it causes an error (on windows,
                // supposedly), this panic could be turned into Ok(0) (temporarily?), and a todo
                // should be added to rethink this behavior
            }
            Err(error) => return Err(error),
        };
        if metadata.len() > o.seek {
            // we're checking for EOF here, hence `>`
            return Ok(0); // skip stage 5.2
        }

        // 5.2. Check for late truncations
        match fs::metadata(&self.path) {
            Ok(metadata) => {
                /*let file_id: UnixFileId = (metadata.dev(), metadata.ino());
                if self.id != file_id {
                    self.status = RotationStatus::Replaced;
                    self.file = None;
                } else*/
                if metadata.len() < o.seek {
                    self.status = RotationStatus::SizeTruncated;
                    self.file = None;
                }
                Ok(0)
            }
            Err(ref error) if error.kind() == ErrorKind::NotFound => {
                self.status = RotationStatus::Unlinked;
                self.file = None;
                Ok(0)
            }
            Err(error) => Err(error),
        }
    }
}
