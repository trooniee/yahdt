use std::env;
use std::io::{self, BufRead, BufReader, Write};
use std::path::PathBuf;
use std::thread::sleep;
use std::time::Duration;
use tailf::Tailf;

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("Usage: {} <filename>", args[0]);
    }

    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    let tailf: Tailf = Tailf::watch(PathBuf::from(&args[1]));
    let mut reader: BufReader<Tailf> = BufReader::new(tailf);
    let mut buf = String::new();

    loop {
        let n = reader.read_line(&mut buf)?;
        if n > 0 {
            // EOF encountered, but it's possible that more data are coming before truncation.
            // So unless EOL is found, wait for either more data, or truncation status.
            if buf.ends_with('\n') {
                stdout.write_all(buf.as_bytes())?;
                stdout.flush()?;
                buf.clear();
            }
        } else {
            let status = reader.get_mut().consume_status().unwrap();
            if status.is_truncated() {
                stdout.write_all(buf.as_bytes())?;
                stdout.flush()?;
                buf.clear();
                eprintln!("^file truncated: {:?}", status);
            } else {
                sleep(Duration::new(0, 100_000_000));
            }
        }
    }
}
