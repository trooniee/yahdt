use std::env;
use std::io::{self, Read, Write};
use std::path::PathBuf;
use std::thread::sleep;
use std::time::Duration;
use tailf::Tailf;

fn main() -> Result<(), io::Error> {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        panic!("Usage: {} <filename>", args[0]);
    }

    let stdout = io::stdout();
    let mut stdout = stdout.lock();
    let mut tailf: Tailf = Tailf::watch(PathBuf::from(&args[1]));
    let mut buf: [u8; 8096] = [0; 8096];

    loop {
        let n = tailf.read(&mut buf)?;
        if n > 0 {
            stdout.write_all(&buf[..n])?;
            stdout.flush()?;
        } else {
            let status = tailf.consume_status().unwrap();
            if status.is_truncated() {
                eprintln!("^file truncated: {:?}", status);
            } else {
                sleep(Duration::new(0, 100_000_000));
            }
        }
    }
}
